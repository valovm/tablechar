var FtpDeploy = require('ftp-deploy');
var ftpDeploy = new FtpDeploy();
 
var config = {
    user: "webkok_table",                   // NOTE that this was username in 1.x 
    password: "qI8&ajM*",           // optional, prompted if none given
    host: "webkok.beget.tech",
    port: 21,
    localRoot: __dirname + "/dist",
    remoteRoot: 'public_html',
    // include: ['*', '**/*'],     // this would upload everything except dot files
    include: ['*'],
    exclude: ['dist/**/*.map'],     // e.g. exclude sourcemaps - ** exclude: [] if nothing to exclude **
    deleteRemote: false,              // delete existing files at destination before uploading
    forcePasv: true                 // Passive mode is forced (EPSV command is not sent)
}

// use with promises
ftpDeploy.deploy(config)
    .then(res => console.log('finished'))
    .catch(err => console.log(err))
    
// use with callback
ftpDeploy.deploy(config, function(err) {
    if (err) console.log(err)
    else console.log('finished');
});