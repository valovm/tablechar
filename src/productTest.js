export default
[
  {
    "code": "7202c110-b",
    "kind": "1",
    "name": "Внутризоновые соединения по сети фиксированной телефонной связи для передачи голосовой информации, факсимильных сообщений и передачи данных",
    "type": "1",
    "actual": "true",
    "values": {
      "value": {
        "qualityDescription": "Да"
      }
    },
    "choiceType": "1",
    "isRequired": "true"
  },
  {
    "code": "edd9ed89-0",
    "kind": "1",
    "name": "Доступ к услугам связи сети связи общего пользования, кроме услуг местной телефонной связи",
    "type": "1",
    "actual": "true",
    "values": {
      "value": {
        "qualityDescription": "Да"
      }
    },
    "choiceType": "1",
    "isRequired": "true"
  },
  {
    "code": "e5e8ba41-c",
    "kind": "1",
    "name": "Доступ к системе информационно-справочного обслуживания",
    "type": "1",
    "actual": "true",
    "values": {
      "value": {
        "qualityDescription": "Да"
      }
    },
    "choiceType": "1",
    "isRequired": "true"
  },
  {
    "code": "89daf8d7-8",
    "kind": "1",
    "name": " Возможности бесплатного круглосуточного вызова экстренных оперативных служб",
    "type": "1",
    "actual": "true",
    "values": {
      "value": {
        "qualityDescription": "Да"
      }
    },
    "choiceType": "1",
    "isRequired": "true"
  },
  {
    "code": "4207a834-3",
    "kind": "1",
    "name": "Доступ к услугам внутризоновой, междугородной и международной телефонной связи",
    "type": "1",
    "actual": "true",
    "values": {
      "value": {
        "qualityDescription": "Да"
      }
    },
    "choiceType": "1",
    "isRequired": "true"
  },
  {
    "code": "c667b433-9",
    "kind": "1",
    "name": "Вид тарификации",
    "type": "1",
    "actual": "true",
    "values": {
      "value": [
        {
          "qualityDescription": "Комбинированная система оплаты"
        }
      ]
    },
    "choiceType": "1",
    "isRequired": "true"
  }
]