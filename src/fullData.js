export default	[{
		"id": 113076,
		"code": "61.10.11.110-00000001",
		"name": "Услуги внутризоновой телефонной связи",
		"data": {
			"data": {
				"NSI": {
					"classifiers": {
						"classifier": {
							"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
							"values": {
								"value": {
									"code": "61.10.11.110",
									"name": "Услуги по предоставлению внутризоновых, междугородных и международных телефонных соединений"
								}
							}
						}
					}
				},
				"code": "61.10.11.110-00000001",
				"name": "Услуги внутризоновой телефонной связи",
				"OKEIs": {
					"OKEI": {
						"code": "876",
						"name": "Условная единица"
					}
				},
				"OKPD2": {
					"code": "61.10.11.110",
					"name": "Услуги по предоставлению внутризоновых, междугородных и международных телефонных соединений"
				},
				"actual": "true",
				"version": "1",
				"isTemplate": "true",
				"publishDate": "2018-10-11T16:00:54+04:00",
				"inclusionDate": "2018-10-11T16:00:54+04:00",
				"characteristics": {
					"characteristic": [{
							"code": "7202c110-b",
							"kind": "1",
							"name": "Внутризоновые соединения по сети фиксированной телефонной связи для передачи голосовой информации, факсимильных сообщений и передачи данных",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "edd9ed89-0",
							"kind": "1",
							"name": "Доступ к услугам связи сети связи общего пользования, кроме услуг местной телефонной связи",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "e5e8ba41-c",
							"kind": "1",
							"name": "Доступ к системе информационно-справочного обслуживания",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "89daf8d7-8",
							"kind": "1",
							"name": " Возможности бесплатного круглосуточного вызова экстренных оперативных служб",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "4207a834-3",
							"kind": "1",
							"name": "Доступ к услугам внутризоновой, междугородной и международной телефонной связи",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "c667b433-9",
							"kind": "1",
							"name": "Вид тарификации",
							"type": "1",
							"actual": "true",
							"values": {
								"value": [{
										"qualityDescription": "Комбинированная система оплаты"
									}, {
										"qualityDescription": "Повременная система оплаты"
									}, {
										"qualityDescription": "Абонентская система оплаты"
									}
								]
							},
							"choiceType": "1",
							"isRequired": "true"
						}
					]
				},
				"applicationDateStart": "2018-11-20T00:00:00+03:00"
			}
		}
	}, {
		"id": 113079,
		"code": "61.10.11.110-00000002",
		"name": "Услуги внутризоновой телефонной связи",
		"data": {
			"data": {
				"NSI": {
					"classifiers": {
						"classifier": {
							"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
							"values": {
								"value": {
									"code": "61.10.11.110",
									"name": "Услуги по предоставлению внутризоновых, междугородных и международных телефонных соединений"
								}
							}
						}
					}
				},
				"code": "61.10.11.110-00000002",
				"name": "Услуги внутризоновой телефонной связи",
				"OKEIs": {
					"OKEI": {
						"code": "876",
						"name": "Условная единица"
					}
				},
				"OKPD2": {
					"code": "61.10.11.110",
					"name": "Услуги по предоставлению внутризоновых, междугородных и международных телефонных соединений"
				},
				"actual": "true",
				"version": "1",
				"isTemplate": "false",
				"publishDate": "2018-10-11T16:00:55+04:00",
				"inclusionDate": "2018-10-11T16:00:55+04:00",
				"characteristics": {
					"characteristic": [{
							"code": "7202c110-b",
							"kind": "1",
							"name": "Внутризоновые соединения по сети фиксированной телефонной связи для передачи голосовой информации, факсимильных сообщений и передачи данных",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "edd9ed89-0",
							"kind": "1",
							"name": "Доступ к услугам связи сети связи общего пользования, кроме услуг местной телефонной связи",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "e5e8ba41-c",
							"kind": "1",
							"name": "Доступ к системе информационно-справочного обслуживания",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "89daf8d7-8",
							"kind": "1",
							"name": " Возможности бесплатного круглосуточного вызова экстренных оперативных служб",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "4207a834-3",
							"kind": "1",
							"name": "Доступ к услугам внутризоновой, междугородной и международной телефонной связи",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "c667b433-9",
							"kind": "1",
							"name": "Вид тарификации",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Комбинированная система оплаты"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}
					]
				},
				"parentPositionInfo": {
					"code": "61.10.11.110-00000001",
					"version": "1"
				},
				"applicationDateStart": "2018-11-20T00:00:00+03:00"
			}
		}
	}, {
		"id": 113078,
		"code": "61.10.11.110-00000003",
		"name": "Услуги внутризоновой телефонной связи",
		"data": {
			"data": {
				"NSI": {
					"classifiers": {
						"classifier": {
							"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
							"values": {
								"value": {
									"code": "61.10.11.110",
									"name": "Услуги по предоставлению внутризоновых, междугородных и международных телефонных соединений"
								}
							}
						}
					}
				},
				"code": "61.10.11.110-00000003",
				"name": "Услуги внутризоновой телефонной связи",
				"OKEIs": {
					"OKEI": {
						"code": "876",
						"name": "Условная единица"
					}
				},
				"OKPD2": {
					"code": "61.10.11.110",
					"name": "Услуги по предоставлению внутризоновых, междугородных и международных телефонных соединений"
				},
				"actual": "true",
				"version": "1",
				"isTemplate": "false",
				"publishDate": "2018-10-11T16:00:55+04:00",
				"inclusionDate": "2018-10-11T16:00:55+04:00",
				"characteristics": {
					"characteristic": [{
							"code": "7202c110-b",
							"kind": "1",
							"name": "Внутризоновые соединения по сети фиксированной телефонной связи для передачи голосовой информации, факсимильных сообщений и передачи данных",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "edd9ed89-0",
							"kind": "1",
							"name": "Доступ к услугам связи сети связи общего пользования, кроме услуг местной телефонной связи",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "e5e8ba41-c",
							"kind": "1",
							"name": "Доступ к системе информационно-справочного обслуживания",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "89daf8d7-8",
							"kind": "1",
							"name": " Возможности бесплатного круглосуточного вызова экстренных оперативных служб",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "4207a834-3",
							"kind": "1",
							"name": "Доступ к услугам внутризоновой, междугородной и международной телефонной связи",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "c667b433-9",
							"kind": "1",
							"name": "Вид тарификации",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Повременная система оплаты"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}
					]
				},
				"parentPositionInfo": {
					"code": "61.10.11.110-00000001",
					"version": "1"
				},
				"applicationDateStart": "2018-11-20T00:00:00+03:00"
			}
		}
	}, {
		"id": 113077,
		"code": "61.10.11.110-00000004",
		"name": "Услуги внутризоновой телефонной связи",
		"data": {
			"data": {
				"NSI": {
					"classifiers": {
						"classifier": {
							"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
							"values": {
								"value": {
									"code": "61.10.11.110",
									"name": "Услуги по предоставлению внутризоновых, междугородных и международных телефонных соединений"
								}
							}
						}
					}
				},
				"code": "61.10.11.110-00000004",
				"name": "Услуги внутризоновой телефонной связи",
				"OKEIs": {
					"OKEI": {
						"code": "876",
						"name": "Условная единица"
					}
				},
				"OKPD2": {
					"code": "61.10.11.110",
					"name": "Услуги по предоставлению внутризоновых, междугородных и международных телефонных соединений"
				},
				"actual": "true",
				"version": "1",
				"isTemplate": "false",
				"publishDate": "2018-10-11T16:00:56+04:00",
				"inclusionDate": "2018-10-11T16:00:56+04:00",
				"characteristics": {
					"characteristic": [{
							"code": "7202c110-b",
							"kind": "1",
							"name": "Внутризоновые соединения по сети фиксированной телефонной связи для передачи голосовой информации, факсимильных сообщений и передачи данных",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "edd9ed89-0",
							"kind": "1",
							"name": "Доступ к услугам связи сети связи общего пользования, кроме услуг местной телефонной связи",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "e5e8ba41-c",
							"kind": "1",
							"name": "Доступ к системе информационно-справочного обслуживания",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "89daf8d7-8",
							"kind": "1",
							"name": " Возможности бесплатного круглосуточного вызова экстренных оперативных служб",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "4207a834-3",
							"kind": "1",
							"name": "Доступ к услугам внутризоновой, междугородной и международной телефонной связи",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "c667b433-9",
							"kind": "1",
							"name": "Вид тарификации",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Абонентская система оплаты"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}
					]
				},
				"parentPositionInfo": {
					"code": "61.10.11.110-00000001",
					"version": "1"
				},
				"applicationDateStart": "2018-11-20T00:00:00+03:00"
			}
		}
	}, {
		"id": 113089,
		"code": "61.10.11.110-00000005",
		"name": "Услуги междугородной телефонной связи",
		"data": {
			"data": {
				"NSI": {
					"classifiers": {
						"classifier": {
							"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
							"values": {
								"value": {
									"code": "61.10.11.110",
									"name": "Услуги по предоставлению внутризоновых, междугородных и международных телефонных соединений"
								}
							}
						}
					}
				},
				"code": "61.10.11.110-00000005",
				"name": "Услуги междугородной телефонной связи",
				"OKEIs": {
					"OKEI": {
						"code": "876",
						"name": "Условная единица"
					}
				},
				"OKPD2": {
					"code": "61.10.11.110",
					"name": "Услуги по предоставлению внутризоновых, междугородных и международных телефонных соединений"
				},
				"actual": "true",
				"version": "1",
				"isTemplate": "true",
				"publishDate": "2018-10-11T16:09:31+04:00",
				"inclusionDate": "2018-10-11T16:09:31+04:00",
				"characteristics": {
					"characteristic": [{
							"code": "6ed23fda-b",
							"kind": "1",
							"name": "Междугородные соединения по сети фиксированной телефонной связи для передачи голосовой информации, факсимильных сообщений и передачи данных",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "a2fc9620-1",
							"kind": "1",
							"name": "Доступ к услугам связи сети связи общего пользования, кроме услуг местной и внутризоновой телефонной связи",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "40b460d6-c",
							"kind": "1",
							"name": "Доступ к системе информационно-справочного обслуживания",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "4203edb0-c",
							"kind": "1",
							"name": "Вид тарификации",
							"type": "1",
							"actual": "true",
							"values": {
								"value": [{
										"qualityDescription": "Комбинированная система оплаты"
									}, {
										"qualityDescription": "Повременная система оплаты"
									}, {
										"qualityDescription": "Абонентская система оплаты"
									}
								]
							},
							"choiceType": "1",
							"isRequired": "true"
						}
					]
				},
				"applicationDateStart": "2018-11-20T00:00:00+03:00"
			}
		}
	}, {
		"id": 113093,
		"code": "61.10.11.110-00000006",
		"name": "Услуги междугородной телефонной связи",
		"data": {
			"data": {
				"NSI": {
					"classifiers": {
						"classifier": {
							"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
							"values": {
								"value": {
									"code": "61.10.11.110",
									"name": "Услуги по предоставлению внутризоновых, междугородных и международных телефонных соединений"
								}
							}
						}
					}
				},
				"code": "61.10.11.110-00000006",
				"name": "Услуги междугородной телефонной связи",
				"OKEIs": {
					"OKEI": {
						"code": "876",
						"name": "Условная единица"
					}
				},
				"OKPD2": {
					"code": "61.10.11.110",
					"name": "Услуги по предоставлению внутризоновых, междугородных и международных телефонных соединений"
				},
				"actual": "true",
				"version": "1",
				"isTemplate": "false",
				"publishDate": "2018-10-11T16:09:31+04:00",
				"inclusionDate": "2018-10-11T16:09:31+04:00",
				"characteristics": {
					"characteristic": [{
							"code": "6ed23fda-b",
							"kind": "1",
							"name": "Междугородные соединения по сети фиксированной телефонной связи для передачи голосовой информации, факсимильных сообщений и передачи данных",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "a2fc9620-1",
							"kind": "1",
							"name": "Доступ к услугам связи сети связи общего пользования, кроме услуг местной и внутризоновой телефонной связи",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "40b460d6-c",
							"kind": "1",
							"name": "Доступ к системе информационно-справочного обслуживания",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "4203edb0-c",
							"kind": "1",
							"name": "Вид тарификации",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Комбинированная система оплаты"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}
					]
				},
				"parentPositionInfo": {
					"code": "61.10.11.110-00000005",
					"version": "1"
				},
				"applicationDateStart": "2018-11-20T00:00:00+03:00"
			}
		}
	}, {
		"id": 113092,
		"code": "61.10.11.110-00000007",
		"name": "Услуги междугородной телефонной связи",
		"data": {
			"data": {
				"NSI": {
					"classifiers": {
						"classifier": {
							"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
							"values": {
								"value": {
									"code": "61.10.11.110",
									"name": "Услуги по предоставлению внутризоновых, междугородных и международных телефонных соединений"
								}
							}
						}
					}
				},
				"code": "61.10.11.110-00000007",
				"name": "Услуги междугородной телефонной связи",
				"OKEIs": {
					"OKEI": {
						"code": "876",
						"name": "Условная единица"
					}
				},
				"OKPD2": {
					"code": "61.10.11.110",
					"name": "Услуги по предоставлению внутризоновых, междугородных и международных телефонных соединений"
				},
				"actual": "true",
				"version": "1",
				"isTemplate": "false",
				"publishDate": "2018-10-11T16:09:32+04:00",
				"inclusionDate": "2018-10-11T16:09:32+04:00",
				"characteristics": {
					"characteristic": [{
							"code": "6ed23fda-b",
							"kind": "1",
							"name": "Междугородные соединения по сети фиксированной телефонной связи для передачи голосовой информации, факсимильных сообщений и передачи данных",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "a2fc9620-1",
							"kind": "1",
							"name": "Доступ к услугам связи сети связи общего пользования, кроме услуг местной и внутризоновой телефонной связи",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "40b460d6-c",
							"kind": "1",
							"name": "Доступ к системе информационно-справочного обслуживания",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "4203edb0-c",
							"kind": "1",
							"name": "Вид тарификации",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Повременная система оплаты"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}
					]
				},
				"parentPositionInfo": {
					"code": "61.10.11.110-00000005",
					"version": "1"
				},
				"applicationDateStart": "2018-11-20T00:00:00+03:00"
			}
		}
	}, {
		"id": 113091,
		"code": "61.10.11.110-00000008",
		"name": "Услуги междугородной телефонной связи",
		"data": {
			"data": {
				"NSI": {
					"classifiers": {
						"classifier": {
							"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
							"values": {
								"value": {
									"code": "61.10.11.110",
									"name": "Услуги по предоставлению внутризоновых, междугородных и международных телефонных соединений"
								}
							}
						}
					}
				},
				"code": "61.10.11.110-00000008",
				"name": "Услуги междугородной телефонной связи",
				"OKEIs": {
					"OKEI": {
						"code": "876",
						"name": "Условная единица"
					}
				},
				"OKPD2": {
					"code": "61.10.11.110",
					"name": "Услуги по предоставлению внутризоновых, междугородных и международных телефонных соединений"
				},
				"actual": "true",
				"version": "1",
				"isTemplate": "false",
				"publishDate": "2018-10-11T16:09:32+04:00",
				"inclusionDate": "2018-10-11T16:09:32+04:00",
				"characteristics": {
					"characteristic": [{
							"code": "6ed23fda-b",
							"kind": "1",
							"name": "Междугородные соединения по сети фиксированной телефонной связи для передачи голосовой информации, факсимильных сообщений и передачи данных",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "a2fc9620-1",
							"kind": "1",
							"name": "Доступ к услугам связи сети связи общего пользования, кроме услуг местной и внутризоновой телефонной связи",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "40b460d6-c",
							"kind": "1",
							"name": "Доступ к системе информационно-справочного обслуживания",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "4203edb0-c",
							"kind": "1",
							"name": "Вид тарификации",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Абонентская система оплаты"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}
					]
				},
				"parentPositionInfo": {
					"code": "61.10.11.110-00000005",
					"version": "1"
				},
				"applicationDateStart": "2018-11-20T00:00:00+03:00"
			}
		}
	}, {
		"id": 113133,
		"code": "61.10.11.110-00000009",
		"name": "Услуги международной телефонной связи",
		"data": {
			"data": {
				"NSI": {
					"classifiers": {
						"classifier": {
							"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
							"values": {
								"value": {
									"code": "61.10.11.110",
									"name": "Услуги по предоставлению внутризоновых, междугородных и международных телефонных соединений"
								}
							}
						}
					}
				},
				"code": "61.10.11.110-00000009",
				"name": "Услуги международной телефонной связи",
				"OKEIs": {
					"OKEI": {
						"code": "876",
						"name": "Условная единица"
					}
				},
				"OKPD2": {
					"code": "61.10.11.110",
					"name": "Услуги по предоставлению внутризоновых, междугородных и международных телефонных соединений"
				},
				"actual": "true",
				"version": "1",
				"isTemplate": "true",
				"publishDate": "2018-10-11T16:11:40+04:00",
				"inclusionDate": "2018-10-11T16:11:40+04:00",
				"characteristics": {
					"characteristic": [{
							"code": "25e550dd-b",
							"kind": "1",
							"name": "Междугородные соединения по сети фиксированной телефонной связи для передачи голосовой информации, факсимильных сообщений и передачи данных",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "a43e243c-b",
							"kind": "1",
							"name": "Доступ к услугам связи сети связи общего пользования, кроме услуг местной, внутризоновой и междугородной телефонной связи",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "35542bd8-d",
							"kind": "1",
							"name": "Доступ к системе информационно-справочного обслуживания",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "eda0892c-9",
							"kind": "1",
							"name": "Вид тарификации",
							"type": "1",
							"actual": "true",
							"values": {
								"value": [{
										"qualityDescription": "Комбинированная система оплаты"
									}, {
										"qualityDescription": "Повременная система оплаты"
									}, {
										"qualityDescription": "Абонентская система оплаты"
									}
								]
							},
							"choiceType": "1",
							"isRequired": "true"
						}
					]
				},
				"applicationDateStart": "2018-11-20T00:00:00+03:00"
			}
		}
	}, {
		"id": 113136,
		"code": "61.10.11.110-00000010",
		"name": "Услуги международной телефонной связи",
		"data": {
			"data": {
				"NSI": {
					"classifiers": {
						"classifier": {
							"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
							"values": {
								"value": {
									"code": "61.10.11.110",
									"name": "Услуги по предоставлению внутризоновых, междугородных и международных телефонных соединений"
								}
							}
						}
					}
				},
				"code": "61.10.11.110-00000010",
				"name": "Услуги международной телефонной связи",
				"OKEIs": {
					"OKEI": {
						"code": "876",
						"name": "Условная единица"
					}
				},
				"OKPD2": {
					"code": "61.10.11.110",
					"name": "Услуги по предоставлению внутризоновых, междугородных и международных телефонных соединений"
				},
				"actual": "true",
				"version": "1",
				"isTemplate": "false",
				"publishDate": "2018-10-11T16:11:41+04:00",
				"inclusionDate": "2018-10-11T16:11:41+04:00",
				"characteristics": {
					"characteristic": [{
							"code": "25e550dd-b",
							"kind": "1",
							"name": "Междугородные соединения по сети фиксированной телефонной связи для передачи голосовой информации, факсимильных сообщений и передачи данных",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "a43e243c-b",
							"kind": "1",
							"name": "Доступ к услугам связи сети связи общего пользования, кроме услуг местной, внутризоновой и междугородной телефонной связи",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "35542bd8-d",
							"kind": "1",
							"name": "Доступ к системе информационно-справочного обслуживания",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "eda0892c-9",
							"kind": "1",
							"name": "Вид тарификации",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Комбинированная система оплаты"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}
					]
				},
				"parentPositionInfo": {
					"code": "61.10.11.110-00000009",
					"version": "1"
				},
				"applicationDateStart": "2018-11-20T00:00:00+03:00"
			}
		}
	}, {
		"id": 113135,
		"code": "61.10.11.110-00000011",
		"name": "Услуги международной телефонной связи",
		"data": {
			"data": {
				"NSI": {
					"classifiers": {
						"classifier": {
							"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
							"values": {
								"value": {
									"code": "61.10.11.110",
									"name": "Услуги по предоставлению внутризоновых, междугородных и международных телефонных соединений"
								}
							}
						}
					}
				},
				"code": "61.10.11.110-00000011",
				"name": "Услуги международной телефонной связи",
				"OKEIs": {
					"OKEI": {
						"code": "876",
						"name": "Условная единица"
					}
				},
				"OKPD2": {
					"code": "61.10.11.110",
					"name": "Услуги по предоставлению внутризоновых, междугородных и международных телефонных соединений"
				},
				"actual": "true",
				"version": "1",
				"isTemplate": "false",
				"publishDate": "2018-10-11T16:11:41+04:00",
				"inclusionDate": "2018-10-11T16:11:41+04:00",
				"characteristics": {
					"characteristic": [{
							"code": "25e550dd-b",
							"kind": "1",
							"name": "Междугородные соединения по сети фиксированной телефонной связи для передачи голосовой информации, факсимильных сообщений и передачи данных",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "a43e243c-b",
							"kind": "1",
							"name": "Доступ к услугам связи сети связи общего пользования, кроме услуг местной, внутризоновой и междугородной телефонной связи",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "35542bd8-d",
							"kind": "1",
							"name": "Доступ к системе информационно-справочного обслуживания",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "eda0892c-9",
							"kind": "1",
							"name": "Вид тарификации",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Повременная система оплаты"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}
					]
				},
				"parentPositionInfo": {
					"code": "61.10.11.110-00000009",
					"version": "1"
				},
				"applicationDateStart": "2018-11-20T00:00:00+03:00"
			}
		}
	}, {
		"id": 113134,
		"code": "61.10.11.110-00000012",
		"name": "Услуги международной телефонной связи",
		"data": {
			"data": {
				"NSI": {
					"classifiers": {
						"classifier": {
							"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
							"values": {
								"value": {
									"code": "61.10.11.110",
									"name": "Услуги по предоставлению внутризоновых, междугородных и международных телефонных соединений"
								}
							}
						}
					}
				},
				"code": "61.10.11.110-00000012",
				"name": "Услуги международной телефонной связи",
				"OKEIs": {
					"OKEI": {
						"code": "876",
						"name": "Условная единица"
					}
				},
				"OKPD2": {
					"code": "61.10.11.110",
					"name": "Услуги по предоставлению внутризоновых, междугородных и международных телефонных соединений"
				},
				"actual": "true",
				"version": "1",
				"isTemplate": "false",
				"publishDate": "2018-10-11T16:11:42+04:00",
				"inclusionDate": "2018-10-11T16:11:42+04:00",
				"characteristics": {
					"characteristic": [{
							"code": "25e550dd-b",
							"kind": "1",
							"name": "Междугородные соединения по сети фиксированной телефонной связи для передачи голосовой информации, факсимильных сообщений и передачи данных",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "a43e243c-b",
							"kind": "1",
							"name": "Доступ к услугам связи сети связи общего пользования, кроме услуг местной, внутризоновой и междугородной телефонной связи",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "35542bd8-d",
							"kind": "1",
							"name": "Доступ к системе информационно-справочного обслуживания",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "eda0892c-9",
							"kind": "1",
							"name": "Вид тарификации",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Абонентская система оплаты"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}
					]
				},
				"parentPositionInfo": {
					"code": "61.10.11.110-00000009",
					"version": "1"
				},
				"applicationDateStart": "2018-11-20T00:00:00+03:00"
			}
		}
	}, {
		"id": 113032,
		"code": "61.10.11.120-00000001",
		"name": "Услуги местной телефонной связи",
		"data": {
			"data": {
				"NSI": {
					"classifiers": {
						"classifier": {
							"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
							"values": {
								"value": {
									"code": "61.10.11.120",
									"name": "Услуги по предоставлению местных соединений"
								}
							}
						}
					}
				},
				"code": "61.10.11.120-00000001",
				"name": "Услуги местной телефонной связи",
				"OKEIs": {
					"OKEI": {
						"code": "876",
						"name": "Условная единица"
					}
				},
				"OKPD2": {
					"code": "61.10.11.120",
					"name": "Услуги по предоставлению местных соединений"
				},
				"actual": "true",
				"version": "1",
				"isTemplate": "true",
				"publishDate": "2018-10-11T15:48:23+04:00",
				"inclusionDate": "2018-10-11T15:48:23+04:00",
				"characteristics": {
					"characteristic": [{
							"code": "e4eb2ceb-6",
							"kind": "1",
							"name": "Доступ к сети связи исполнителя (оператора)",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "49dc7f3e-a",
							"kind": "1",
							"name": "Абонентская линия в постоянное пользование",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "2aed1d9f-e",
							"kind": "1",
							"name": "Местные телефонные соединения по сети фиксированной телефонной связи с использованием пользовательского (оконечного) оборудования с выделением абоненту номера (номеров) из плана нумерации сети местной телефонной связи для передачи голосовой информации, факсимильных сообщений и передачи данных",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "37c56ad7-b",
							"kind": "1",
							"name": "Доступ к услугам связи сети связи общего пользования",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "32834541-5",
							"kind": "1",
							"name": "Доступ к системе информационно-справочного обслуживания",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "532188c3-d",
							"kind": "1",
							"name": " Возможности бесплатного круглосуточного вызова экстренных оперативных служб",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "dbad0b1a-c",
							"kind": "1",
							"name": "Доступ к услугам внутризоновой, междугородной и международной телефонной связи",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "937ac388-4",
							"kind": "2",
							"name": "Ожидание вызова",
							"type": "1",
							"actual": "true",
							"values": {
								"value": [{
										"qualityDescription": "Нет"
									}, {
										"qualityDescription": "Да"
									}
								]
							},
							"choiceType": "2",
							"isRequired": "false"
						}, {
							"code": "136246bd-9",
							"kind": "2",
							"name": "Переадресация",
							"type": "1",
							"actual": "true",
							"values": {
								"value": [{
										"qualityDescription": "Нет"
									}, {
										"qualityDescription": "Да"
									}
								]
							},
							"choiceType": "2",
							"isRequired": "false"
						}, {
							"code": "c6faf83c-7",
							"kind": "2",
							"name": "Автоматическое определение номера",
							"type": "1",
							"actual": "true",
							"values": {
								"value": [{
										"qualityDescription": "Нет"
									}, {
										"qualityDescription": "Да"
									}
								]
							},
							"choiceType": "2",
							"isRequired": "false"
						}, {
							"code": "f47e41a8-b",
							"kind": "2",
							"name": "Конференц-связь",
							"type": "1",
							"actual": "true",
							"values": {
								"value": [{
										"qualityDescription": "Нет"
									}, {
										"qualityDescription": "Да"
									}
								]
							},
							"choiceType": "2",
							"isRequired": "false"
						}, {
							"code": "fcf8d951-9",
							"kind": "2",
							"name": "Обратный вызов",
							"type": "1",
							"actual": "true",
							"values": {
								"value": [{
										"qualityDescription": "Нет"
									}, {
										"qualityDescription": "Да"
									}
								]
							},
							"choiceType": "2",
							"isRequired": "false"
						}, {
							"code": "c041fa18-1",
							"kind": "2",
							"name": "Ограничения вызовов",
							"type": "1",
							"actual": "true",
							"values": {
								"value": [{
										"qualityDescription": "Нет"
									}, {
										"qualityDescription": "Да"
									}
								]
							},
							"choiceType": "2",
							"isRequired": "false"
						}, {
							"code": "0f27885f-d",
							"kind": "2",
							"name": "Голосовая почта",
							"type": "1",
							"actual": "true",
							"values": {
								"value": [{
										"qualityDescription": "Нет"
									}, {
										"qualityDescription": "Да"
									}
								]
							},
							"choiceType": "2",
							"isRequired": "false"
						}, {
							"code": "4bfe1818-2",
							"kind": "2",
							"name": "Голосовое меню",
							"type": "1",
							"actual": "true",
							"values": {
								"value": [{
										"qualityDescription": "Нет"
									}, {
										"qualityDescription": "Да"
									}
								]
							},
							"choiceType": "2",
							"isRequired": "false"
						}, {
							"code": "3d2cf408-5",
							"kind": "2",
							"name": "Многоканальный номер",
							"type": "1",
							"actual": "true",
							"values": {
								"value": [{
										"qualityDescription": "Нет"
									}, {
										"qualityDescription": "Да"
									}
								]
							},
							"choiceType": "2",
							"isRequired": "false"
						}, {
							"code": "bf0b9c4c-2",
							"kind": "1",
							"name": "В зависимости от набранного номера в сети осуществляются операции по установлению одного из вариантов местного телефонного соединения",
							"type": "1",
							"actual": "true",
							"values": {
								"value": [{
										"qualityDescription": "С точкой присоединения к зоновой сети (если требуется осуществить доступ к услугам внутризоновой, междугородной или международной телефонной связи)"
									}, {
										"qualityDescription": " С точкой присоединения к местной сети другого оператора (если местное соединение требуется осуществить через сети двух или более операторов)"
									}, {
										"qualityDescription": " С оконечным устройством вызываемого абонента (службы) в пределах местной сети единственного оператора"
									}
								]
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "81a6395e-4",
							"kind": "1",
							"name": "Вид тарификации",
							"type": "1",
							"actual": "true",
							"values": {
								"value": [{
										"qualityDescription": "Комбинированная система оплаты"
									}, {
										"qualityDescription": "Повременная система оплаты"
									}, {
										"qualityDescription": "Абонентская система оплаты"
									}
								]
							},
							"choiceType": "1",
							"isRequired": "true"
						}
					]
				},
				"applicationDateStart": "2018-11-20T00:00:00+03:00"
			}
		}
	}, {
		"id": 113064,
		"code": "61.10.11.120-00000002",
		"name": "Услуги местной телефонной связи",
		"data": {
			"data": {
				"NSI": {
					"classifiers": {
						"classifier": {
							"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
							"values": {
								"value": {
									"code": "61.10.11.120",
									"name": "Услуги по предоставлению местных соединений"
								}
							}
						}
					}
				},
				"code": "61.10.11.120-00000002",
				"name": "Услуги местной телефонной связи",
				"OKEIs": {
					"OKEI": {
						"code": "876",
						"name": "Условная единица"
					}
				},
				"OKPD2": {
					"code": "61.10.11.120",
					"name": "Услуги по предоставлению местных соединений"
				},
				"actual": "true",
				"version": "1",
				"isTemplate": "false",
				"publishDate": "2018-10-11T15:48:24+04:00",
				"inclusionDate": "2018-10-11T15:48:24+04:00",
				"characteristics": {
					"characteristic": [{
							"code": "e4eb2ceb-6",
							"kind": "1",
							"name": "Доступ к сети связи исполнителя (оператора)",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "49dc7f3e-a",
							"kind": "1",
							"name": "Абонентская линия в постоянное пользование",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "2aed1d9f-e",
							"kind": "1",
							"name": "Местные телефонные соединения по сети фиксированной телефонной связи с использованием пользовательского (оконечного) оборудования с выделением абоненту номера (номеров) из плана нумерации сети местной телефонной связи для передачи голосовой информации, факсимильных сообщений и передачи данных",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "37c56ad7-b",
							"kind": "1",
							"name": "Доступ к услугам связи сети связи общего пользования",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "32834541-5",
							"kind": "1",
							"name": "Доступ к системе информационно-справочного обслуживания",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "532188c3-d",
							"kind": "1",
							"name": " Возможности бесплатного круглосуточного вызова экстренных оперативных служб",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "dbad0b1a-c",
							"kind": "1",
							"name": "Доступ к услугам внутризоновой, междугородной и международной телефонной связи",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "937ac388-4",
							"kind": "2",
							"name": "Ожидание вызова",
							"type": "1",
							"actual": "true",
							"values": {
								"value": [{
										"qualityDescription": "Нет"
									}, {
										"qualityDescription": "Да"
									}
								]
							},
							"choiceType": "2",
							"isRequired": "false"
						}, {
							"code": "136246bd-9",
							"kind": "2",
							"name": "Переадресация",
							"type": "1",
							"actual": "true",
							"values": {
								"value": [{
										"qualityDescription": "Нет"
									}, {
										"qualityDescription": "Да"
									}
								]
							},
							"choiceType": "2",
							"isRequired": "false"
						}, {
							"code": "c6faf83c-7",
							"kind": "2",
							"name": "Автоматическое определение номера",
							"type": "1",
							"actual": "true",
							"values": {
								"value": [{
										"qualityDescription": "Нет"
									}, {
										"qualityDescription": "Да"
									}
								]
							},
							"choiceType": "2",
							"isRequired": "false"
						}, {
							"code": "f47e41a8-b",
							"kind": "2",
							"name": "Конференц-связь",
							"type": "1",
							"actual": "true",
							"values": {
								"value": [{
										"qualityDescription": "Нет"
									}, {
										"qualityDescription": "Да"
									}
								]
							},
							"choiceType": "2",
							"isRequired": "false"
						}, {
							"code": "fcf8d951-9",
							"kind": "2",
							"name": "Обратный вызов",
							"type": "1",
							"actual": "true",
							"values": {
								"value": [{
										"qualityDescription": "Нет"
									}, {
										"qualityDescription": "Да"
									}
								]
							},
							"choiceType": "2",
							"isRequired": "false"
						}, {
							"code": "c041fa18-1",
							"kind": "2",
							"name": "Ограничения вызовов",
							"type": "1",
							"actual": "true",
							"values": {
								"value": [{
										"qualityDescription": "Нет"
									}, {
										"qualityDescription": "Да"
									}
								]
							},
							"choiceType": "2",
							"isRequired": "false"
						}, {
							"code": "0f27885f-d",
							"kind": "2",
							"name": "Голосовая почта",
							"type": "1",
							"actual": "true",
							"values": {
								"value": [{
										"qualityDescription": "Нет"
									}, {
										"qualityDescription": "Да"
									}
								]
							},
							"choiceType": "2",
							"isRequired": "false"
						}, {
							"code": "4bfe1818-2",
							"kind": "2",
							"name": "Голосовое меню",
							"type": "1",
							"actual": "true",
							"values": {
								"value": [{
										"qualityDescription": "Нет"
									}, {
										"qualityDescription": "Да"
									}
								]
							},
							"choiceType": "2",
							"isRequired": "false"
						}, {
							"code": "3d2cf408-5",
							"kind": "2",
							"name": "Многоканальный номер",
							"type": "1",
							"actual": "true",
							"values": {
								"value": [{
										"qualityDescription": "Нет"
									}, {
										"qualityDescription": "Да"
									}
								]
							},
							"choiceType": "2",
							"isRequired": "false"
						}, {
							"code": "bf0b9c4c-2",
							"kind": "1",
							"name": "В зависимости от набранного номера в сети осуществляются операции по установлению одного из вариантов местного телефонного соединения",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "С точкой присоединения к зоновой сети (если требуется осуществить доступ к услугам внутризоновой, междугородной или международной телефонной связи)"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "81a6395e-4",
							"kind": "1",
							"name": "Вид тарификации",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Комбинированная система оплаты"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}
					]
				},
				"parentPositionInfo": {
					"code": "61.10.11.120-00000001",
					"version": "1"
				},
				"applicationDateStart": "2018-11-20T00:00:00+03:00"
			}
		}
	}, {
		"id": 113063,
		"code": "61.10.11.120-00000003",
		"name": "Услуги местной телефонной связи",
		"data": {
			"data": {
				"NSI": {
					"classifiers": {
						"classifier": {
							"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
							"values": {
								"value": {
									"code": "61.10.11.120",
									"name": "Услуги по предоставлению местных соединений"
								}
							}
						}
					}
				},
				"code": "61.10.11.120-00000003",
				"name": "Услуги местной телефонной связи",
				"OKEIs": {
					"OKEI": {
						"code": "876",
						"name": "Условная единица"
					}
				},
				"OKPD2": {
					"code": "61.10.11.120",
					"name": "Услуги по предоставлению местных соединений"
				},
				"actual": "true",
				"version": "1",
				"isTemplate": "false",
				"publishDate": "2018-10-11T15:48:25+04:00",
				"inclusionDate": "2018-10-11T15:48:25+04:00",
				"characteristics": {
					"characteristic": [{
							"code": "e4eb2ceb-6",
							"kind": "1",
							"name": "Доступ к сети связи исполнителя (оператора)",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "49dc7f3e-a",
							"kind": "1",
							"name": "Абонентская линия в постоянное пользование",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "2aed1d9f-e",
							"kind": "1",
							"name": "Местные телефонные соединения по сети фиксированной телефонной связи с использованием пользовательского (оконечного) оборудования с выделением абоненту номера (номеров) из плана нумерации сети местной телефонной связи для передачи голосовой информации, факсимильных сообщений и передачи данных",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "37c56ad7-b",
							"kind": "1",
							"name": "Доступ к услугам связи сети связи общего пользования",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "32834541-5",
							"kind": "1",
							"name": "Доступ к системе информационно-справочного обслуживания",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "532188c3-d",
							"kind": "1",
							"name": " Возможности бесплатного круглосуточного вызова экстренных оперативных служб",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "dbad0b1a-c",
							"kind": "1",
							"name": "Доступ к услугам внутризоновой, междугородной и международной телефонной связи",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "937ac388-4",
							"kind": "2",
							"name": "Ожидание вызова",
							"type": "1",
							"actual": "true",
							"values": {
								"value": [{
										"qualityDescription": "Нет"
									}, {
										"qualityDescription": "Да"
									}
								]
							},
							"choiceType": "2",
							"isRequired": "false"
						}, {
							"code": "136246bd-9",
							"kind": "2",
							"name": "Переадресация",
							"type": "1",
							"actual": "true",
							"values": {
								"value": [{
										"qualityDescription": "Нет"
									}, {
										"qualityDescription": "Да"
									}
								]
							},
							"choiceType": "2",
							"isRequired": "false"
						}, {
							"code": "c6faf83c-7",
							"kind": "2",
							"name": "Автоматическое определение номера",
							"type": "1",
							"actual": "true",
							"values": {
								"value": [{
										"qualityDescription": "Нет"
									}, {
										"qualityDescription": "Да"
									}
								]
							},
							"choiceType": "2",
							"isRequired": "false"
						}, {
							"code": "f47e41a8-b",
							"kind": "2",
							"name": "Конференц-связь",
							"type": "1",
							"actual": "true",
							"values": {
								"value": [{
										"qualityDescription": "Нет"
									}, {
										"qualityDescription": "Да"
									}
								]
							},
							"choiceType": "2",
							"isRequired": "false"
						}, {
							"code": "fcf8d951-9",
							"kind": "2",
							"name": "Обратный вызов",
							"type": "1",
							"actual": "true",
							"values": {
								"value": [{
										"qualityDescription": "Нет"
									}, {
										"qualityDescription": "Да"
									}
								]
							},
							"choiceType": "2",
							"isRequired": "false"
						}, {
							"code": "c041fa18-1",
							"kind": "2",
							"name": "Ограничения вызовов",
							"type": "1",
							"actual": "true",
							"values": {
								"value": [{
										"qualityDescription": "Нет"
									}, {
										"qualityDescription": "Да"
									}
								]
							},
							"choiceType": "2",
							"isRequired": "false"
						}, {
							"code": "0f27885f-d",
							"kind": "2",
							"name": "Голосовая почта",
							"type": "1",
							"actual": "true",
							"values": {
								"value": [{
										"qualityDescription": "Нет"
									}, {
										"qualityDescription": "Да"
									}
								]
							},
							"choiceType": "2",
							"isRequired": "false"
						}, {
							"code": "4bfe1818-2",
							"kind": "2",
							"name": "Голосовое меню",
							"type": "1",
							"actual": "true",
							"values": {
								"value": [{
										"qualityDescription": "Нет"
									}, {
										"qualityDescription": "Да"
									}
								]
							},
							"choiceType": "2",
							"isRequired": "false"
						}, {
							"code": "3d2cf408-5",
							"kind": "2",
							"name": "Многоканальный номер",
							"type": "1",
							"actual": "true",
							"values": {
								"value": [{
										"qualityDescription": "Нет"
									}, {
										"qualityDescription": "Да"
									}
								]
							},
							"choiceType": "2",
							"isRequired": "false"
						}, {
							"code": "bf0b9c4c-2",
							"kind": "1",
							"name": "В зависимости от набранного номера в сети осуществляются операции по установлению одного из вариантов местного телефонного соединения",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "С точкой присоединения к зоновой сети (если требуется осуществить доступ к услугам внутризоновой, междугородной или международной телефонной связи)"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "81a6395e-4",
							"kind": "1",
							"name": "Вид тарификации",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Повременная система оплаты"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}
					]
				},
				"parentPositionInfo": {
					"code": "61.10.11.120-00000001",
					"version": "1"
				},
				"applicationDateStart": "2018-11-20T00:00:00+03:00"
			}
		}
	}, {
		"id": 113056,
		"code": "61.10.11.120-00000004",
		"name": "Услуги местной телефонной связи",
		"data": {
			"data": {
				"NSI": {
					"classifiers": {
						"classifier": {
							"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
							"values": {
								"value": {
									"code": "61.10.11.120",
									"name": "Услуги по предоставлению местных соединений"
								}
							}
						}
					}
				},
				"code": "61.10.11.120-00000004",
				"name": "Услуги местной телефонной связи",
				"OKEIs": {
					"OKEI": {
						"code": "876",
						"name": "Условная единица"
					}
				},
				"OKPD2": {
					"code": "61.10.11.120",
					"name": "Услуги по предоставлению местных соединений"
				},
				"actual": "true",
				"version": "1",
				"isTemplate": "false",
				"publishDate": "2018-10-11T15:48:26+04:00",
				"inclusionDate": "2018-10-11T15:48:26+04:00",
				"characteristics": {
					"characteristic": [{
							"code": "e4eb2ceb-6",
							"kind": "1",
							"name": "Доступ к сети связи исполнителя (оператора)",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "49dc7f3e-a",
							"kind": "1",
							"name": "Абонентская линия в постоянное пользование",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "2aed1d9f-e",
							"kind": "1",
							"name": "Местные телефонные соединения по сети фиксированной телефонной связи с использованием пользовательского (оконечного) оборудования с выделением абоненту номера (номеров) из плана нумерации сети местной телефонной связи для передачи голосовой информации, факсимильных сообщений и передачи данных",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "37c56ad7-b",
							"kind": "1",
							"name": "Доступ к услугам связи сети связи общего пользования",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "32834541-5",
							"kind": "1",
							"name": "Доступ к системе информационно-справочного обслуживания",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "532188c3-d",
							"kind": "1",
							"name": " Возможности бесплатного круглосуточного вызова экстренных оперативных служб",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "dbad0b1a-c",
							"kind": "1",
							"name": "Доступ к услугам внутризоновой, междугородной и международной телефонной связи",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "937ac388-4",
							"kind": "2",
							"name": "Ожидание вызова",
							"type": "1",
							"actual": "true",
							"values": {
								"value": [{
										"qualityDescription": "Нет"
									}, {
										"qualityDescription": "Да"
									}
								]
							},
							"choiceType": "2",
							"isRequired": "false"
						}, {
							"code": "136246bd-9",
							"kind": "2",
							"name": "Переадресация",
							"type": "1",
							"actual": "true",
							"values": {
								"value": [{
										"qualityDescription": "Нет"
									}, {
										"qualityDescription": "Да"
									}
								]
							},
							"choiceType": "2",
							"isRequired": "false"
						}, {
							"code": "c6faf83c-7",
							"kind": "2",
							"name": "Автоматическое определение номера",
							"type": "1",
							"actual": "true",
							"values": {
								"value": [{
										"qualityDescription": "Нет"
									}, {
										"qualityDescription": "Да"
									}
								]
							},
							"choiceType": "2",
							"isRequired": "false"
						}, {
							"code": "f47e41a8-b",
							"kind": "2",
							"name": "Конференц-связь",
							"type": "1",
							"actual": "true",
							"values": {
								"value": [{
										"qualityDescription": "Нет"
									}, {
										"qualityDescription": "Да"
									}
								]
							},
							"choiceType": "2",
							"isRequired": "false"
						}, {
							"code": "fcf8d951-9",
							"kind": "2",
							"name": "Обратный вызов",
							"type": "1",
							"actual": "true",
							"values": {
								"value": [{
										"qualityDescription": "Нет"
									}, {
										"qualityDescription": "Да"
									}
								]
							},
							"choiceType": "2",
							"isRequired": "false"
						}, {
							"code": "c041fa18-1",
							"kind": "2",
							"name": "Ограничения вызовов",
							"type": "1",
							"actual": "true",
							"values": {
								"value": [{
										"qualityDescription": "Нет"
									}, {
										"qualityDescription": "Да"
									}
								]
							},
							"choiceType": "2",
							"isRequired": "false"
						}, {
							"code": "0f27885f-d",
							"kind": "2",
							"name": "Голосовая почта",
							"type": "1",
							"actual": "true",
							"values": {
								"value": [{
										"qualityDescription": "Нет"
									}, {
										"qualityDescription": "Да"
									}
								]
							},
							"choiceType": "2",
							"isRequired": "false"
						}, {
							"code": "4bfe1818-2",
							"kind": "2",
							"name": "Голосовое меню",
							"type": "1",
							"actual": "true",
							"values": {
								"value": [{
										"qualityDescription": "Нет"
									}, {
										"qualityDescription": "Да"
									}
								]
							},
							"choiceType": "2",
							"isRequired": "false"
						}, {
							"code": "3d2cf408-5",
							"kind": "2",
							"name": "Многоканальный номер",
							"type": "1",
							"actual": "true",
							"values": {
								"value": [{
										"qualityDescription": "Нет"
									}, {
										"qualityDescription": "Да"
									}
								]
							},
							"choiceType": "2",
							"isRequired": "false"
						}, {
							"code": "bf0b9c4c-2",
							"kind": "1",
							"name": "В зависимости от набранного номера в сети осуществляются операции по установлению одного из вариантов местного телефонного соединения",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "С точкой присоединения к зоновой сети (если требуется осуществить доступ к услугам внутризоновой, междугородной или международной телефонной связи)"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "81a6395e-4",
							"kind": "1",
							"name": "Вид тарификации",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Абонентская система оплаты"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}
					]
				},
				"parentPositionInfo": {
					"code": "61.10.11.120-00000001",
					"version": "1"
				},
				"applicationDateStart": "2018-11-20T00:00:00+03:00"
			}
		}
	}, {
		"id": 113051,
		"code": "61.10.11.120-00000005",
		"name": "Услуги местной телефонной связи",
		"data": {
			"data": {
				"NSI": {
					"classifiers": {
						"classifier": {
							"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
							"values": {
								"value": {
									"code": "61.10.11.120",
									"name": "Услуги по предоставлению местных соединений"
								}
							}
						}
					}
				},
				"code": "61.10.11.120-00000005",
				"name": "Услуги местной телефонной связи",
				"OKEIs": {
					"OKEI": {
						"code": "876",
						"name": "Условная единица"
					}
				},
				"OKPD2": {
					"code": "61.10.11.120",
					"name": "Услуги по предоставлению местных соединений"
				},
				"actual": "true",
				"version": "1",
				"isTemplate": "false",
				"publishDate": "2018-10-11T15:48:27+04:00",
				"inclusionDate": "2018-10-11T15:48:27+04:00",
				"characteristics": {
					"characteristic": [{
							"code": "e4eb2ceb-6",
							"kind": "1",
							"name": "Доступ к сети связи исполнителя (оператора)",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "49dc7f3e-a",
							"kind": "1",
							"name": "Абонентская линия в постоянное пользование",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "2aed1d9f-e",
							"kind": "1",
							"name": "Местные телефонные соединения по сети фиксированной телефонной связи с использованием пользовательского (оконечного) оборудования с выделением абоненту номера (номеров) из плана нумерации сети местной телефонной связи для передачи голосовой информации, факсимильных сообщений и передачи данных",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "37c56ad7-b",
							"kind": "1",
							"name": "Доступ к услугам связи сети связи общего пользования",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "32834541-5",
							"kind": "1",
							"name": "Доступ к системе информационно-справочного обслуживания",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "532188c3-d",
							"kind": "1",
							"name": " Возможности бесплатного круглосуточного вызова экстренных оперативных служб",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "dbad0b1a-c",
							"kind": "1",
							"name": "Доступ к услугам внутризоновой, междугородной и международной телефонной связи",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "937ac388-4",
							"kind": "2",
							"name": "Ожидание вызова",
							"type": "1",
							"actual": "true",
							"values": {
								"value": [{
										"qualityDescription": "Нет"
									}, {
										"qualityDescription": "Да"
									}
								]
							},
							"choiceType": "2",
							"isRequired": "false"
						}, {
							"code": "136246bd-9",
							"kind": "2",
							"name": "Переадресация",
							"type": "1",
							"actual": "true",
							"values": {
								"value": [{
										"qualityDescription": "Нет"
									}, {
										"qualityDescription": "Да"
									}
								]
							},
							"choiceType": "2",
							"isRequired": "false"
						}, {
							"code": "c6faf83c-7",
							"kind": "2",
							"name": "Автоматическое определение номера",
							"type": "1",
							"actual": "true",
							"values": {
								"value": [{
										"qualityDescription": "Нет"
									}, {
										"qualityDescription": "Да"
									}
								]
							},
							"choiceType": "2",
							"isRequired": "false"
						}, {
							"code": "f47e41a8-b",
							"kind": "2",
							"name": "Конференц-связь",
							"type": "1",
							"actual": "true",
							"values": {
								"value": [{
										"qualityDescription": "Нет"
									}, {
										"qualityDescription": "Да"
									}
								]
							},
							"choiceType": "2",
							"isRequired": "false"
						}, {
							"code": "fcf8d951-9",
							"kind": "2",
							"name": "Обратный вызов",
							"type": "1",
							"actual": "true",
							"values": {
								"value": [{
										"qualityDescription": "Нет"
									}, {
										"qualityDescription": "Да"
									}
								]
							},
							"choiceType": "2",
							"isRequired": "false"
						}, {
							"code": "c041fa18-1",
							"kind": "2",
							"name": "Ограничения вызовов",
							"type": "1",
							"actual": "true",
							"values": {
								"value": [{
										"qualityDescription": "Нет"
									}, {
										"qualityDescription": "Да"
									}
								]
							},
							"choiceType": "2",
							"isRequired": "false"
						}, {
							"code": "0f27885f-d",
							"kind": "2",
							"name": "Голосовая почта",
							"type": "1",
							"actual": "true",
							"values": {
								"value": [{
										"qualityDescription": "Нет"
									}, {
										"qualityDescription": "Да"
									}
								]
							},
							"choiceType": "2",
							"isRequired": "false"
						}, {
							"code": "4bfe1818-2",
							"kind": "2",
							"name": "Голосовое меню",
							"type": "1",
							"actual": "true",
							"values": {
								"value": [{
										"qualityDescription": "Нет"
									}, {
										"qualityDescription": "Да"
									}
								]
							},
							"choiceType": "2",
							"isRequired": "false"
						}, {
							"code": "3d2cf408-5",
							"kind": "2",
							"name": "Многоканальный номер",
							"type": "1",
							"actual": "true",
							"values": {
								"value": [{
										"qualityDescription": "Нет"
									}, {
										"qualityDescription": "Да"
									}
								]
							},
							"choiceType": "2",
							"isRequired": "false"
						}, {
							"code": "bf0b9c4c-2",
							"kind": "1",
							"name": "В зависимости от набранного номера в сети осуществляются операции по установлению одного из вариантов местного телефонного соединения",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": " С точкой присоединения к местной сети другого оператора (если местное соединение требуется осуществить через сети двух или более операторов)"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "81a6395e-4",
							"kind": "1",
							"name": "Вид тарификации",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Комбинированная система оплаты"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}
					]
				},
				"parentPositionInfo": {
					"code": "61.10.11.120-00000001",
					"version": "1"
				},
				"applicationDateStart": "2018-11-20T00:00:00+03:00"
			}
		}
	}, {
		"id": 113048,
		"code": "61.10.11.120-00000006",
		"name": "Услуги местной телефонной связи",
		"data": {
			"data": {
				"NSI": {
					"classifiers": {
						"classifier": {
							"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
							"values": {
								"value": {
									"code": "61.10.11.120",
									"name": "Услуги по предоставлению местных соединений"
								}
							}
						}
					}
				},
				"code": "61.10.11.120-00000006",
				"name": "Услуги местной телефонной связи",
				"OKEIs": {
					"OKEI": {
						"code": "876",
						"name": "Условная единица"
					}
				},
				"OKPD2": {
					"code": "61.10.11.120",
					"name": "Услуги по предоставлению местных соединений"
				},
				"actual": "true",
				"version": "1",
				"isTemplate": "false",
				"publishDate": "2018-10-11T15:48:28+04:00",
				"inclusionDate": "2018-10-11T15:48:28+04:00",
				"characteristics": {
					"characteristic": [{
							"code": "e4eb2ceb-6",
							"kind": "1",
							"name": "Доступ к сети связи исполнителя (оператора)",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "49dc7f3e-a",
							"kind": "1",
							"name": "Абонентская линия в постоянное пользование",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "2aed1d9f-e",
							"kind": "1",
							"name": "Местные телефонные соединения по сети фиксированной телефонной связи с использованием пользовательского (оконечного) оборудования с выделением абоненту номера (номеров) из плана нумерации сети местной телефонной связи для передачи голосовой информации, факсимильных сообщений и передачи данных",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "37c56ad7-b",
							"kind": "1",
							"name": "Доступ к услугам связи сети связи общего пользования",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "32834541-5",
							"kind": "1",
							"name": "Доступ к системе информационно-справочного обслуживания",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "532188c3-d",
							"kind": "1",
							"name": " Возможности бесплатного круглосуточного вызова экстренных оперативных служб",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "dbad0b1a-c",
							"kind": "1",
							"name": "Доступ к услугам внутризоновой, междугородной и международной телефонной связи",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "937ac388-4",
							"kind": "2",
							"name": "Ожидание вызова",
							"type": "1",
							"actual": "true",
							"values": {
								"value": [{
										"qualityDescription": "Нет"
									}, {
										"qualityDescription": "Да"
									}
								]
							},
							"choiceType": "2",
							"isRequired": "false"
						}, {
							"code": "136246bd-9",
							"kind": "2",
							"name": "Переадресация",
							"type": "1",
							"actual": "true",
							"values": {
								"value": [{
										"qualityDescription": "Нет"
									}, {
										"qualityDescription": "Да"
									}
								]
							},
							"choiceType": "2",
							"isRequired": "false"
						}, {
							"code": "c6faf83c-7",
							"kind": "2",
							"name": "Автоматическое определение номера",
							"type": "1",
							"actual": "true",
							"values": {
								"value": [{
										"qualityDescription": "Нет"
									}, {
										"qualityDescription": "Да"
									}
								]
							},
							"choiceType": "2",
							"isRequired": "false"
						}, {
							"code": "f47e41a8-b",
							"kind": "2",
							"name": "Конференц-связь",
							"type": "1",
							"actual": "true",
							"values": {
								"value": [{
										"qualityDescription": "Нет"
									}, {
										"qualityDescription": "Да"
									}
								]
							},
							"choiceType": "2",
							"isRequired": "false"
						}, {
							"code": "fcf8d951-9",
							"kind": "2",
							"name": "Обратный вызов",
							"type": "1",
							"actual": "true",
							"values": {
								"value": [{
										"qualityDescription": "Нет"
									}, {
										"qualityDescription": "Да"
									}
								]
							},
							"choiceType": "2",
							"isRequired": "false"
						}, {
							"code": "c041fa18-1",
							"kind": "2",
							"name": "Ограничения вызовов",
							"type": "1",
							"actual": "true",
							"values": {
								"value": [{
										"qualityDescription": "Нет"
									}, {
										"qualityDescription": "Да"
									}
								]
							},
							"choiceType": "2",
							"isRequired": "false"
						}, {
							"code": "0f27885f-d",
							"kind": "2",
							"name": "Голосовая почта",
							"type": "1",
							"actual": "true",
							"values": {
								"value": [{
										"qualityDescription": "Нет"
									}, {
										"qualityDescription": "Да"
									}
								]
							},
							"choiceType": "2",
							"isRequired": "false"
						}, {
							"code": "4bfe1818-2",
							"kind": "2",
							"name": "Голосовое меню",
							"type": "1",
							"actual": "true",
							"values": {
								"value": [{
										"qualityDescription": "Нет"
									}, {
										"qualityDescription": "Да"
									}
								]
							},
							"choiceType": "2",
							"isRequired": "false"
						}, {
							"code": "3d2cf408-5",
							"kind": "2",
							"name": "Многоканальный номер",
							"type": "1",
							"actual": "true",
							"values": {
								"value": [{
										"qualityDescription": "Нет"
									}, {
										"qualityDescription": "Да"
									}
								]
							},
							"choiceType": "2",
							"isRequired": "false"
						}, {
							"code": "bf0b9c4c-2",
							"kind": "1",
							"name": "В зависимости от набранного номера в сети осуществляются операции по установлению одного из вариантов местного телефонного соединения",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": " С точкой присоединения к местной сети другого оператора (если местное соединение требуется осуществить через сети двух или более операторов)"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "81a6395e-4",
							"kind": "1",
							"name": "Вид тарификации",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Повременная система оплаты"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}
					]
				},
				"parentPositionInfo": {
					"code": "61.10.11.120-00000001",
					"version": "1"
				},
				"applicationDateStart": "2018-11-20T00:00:00+03:00"
			}
		}
	}, {
		"id": 113043,
		"code": "61.10.11.120-00000007",
		"name": "Услуги местной телефонной связи",
		"data": {
			"data": {
				"NSI": {
					"classifiers": {
						"classifier": {
							"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
							"values": {
								"value": {
									"code": "61.10.11.120",
									"name": "Услуги по предоставлению местных соединений"
								}
							}
						}
					}
				},
				"code": "61.10.11.120-00000007",
				"name": "Услуги местной телефонной связи",
				"OKEIs": {
					"OKEI": {
						"code": "876",
						"name": "Условная единица"
					}
				},
				"OKPD2": {
					"code": "61.10.11.120",
					"name": "Услуги по предоставлению местных соединений"
				},
				"actual": "true",
				"version": "1",
				"isTemplate": "false",
				"publishDate": "2018-10-11T15:48:29+04:00",
				"inclusionDate": "2018-10-11T15:48:29+04:00",
				"characteristics": {
					"characteristic": [{
							"code": "e4eb2ceb-6",
							"kind": "1",
							"name": "Доступ к сети связи исполнителя (оператора)",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "49dc7f3e-a",
							"kind": "1",
							"name": "Абонентская линия в постоянное пользование",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "2aed1d9f-e",
							"kind": "1",
							"name": "Местные телефонные соединения по сети фиксированной телефонной связи с использованием пользовательского (оконечного) оборудования с выделением абоненту номера (номеров) из плана нумерации сети местной телефонной связи для передачи голосовой информации, факсимильных сообщений и передачи данных",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "37c56ad7-b",
							"kind": "1",
							"name": "Доступ к услугам связи сети связи общего пользования",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "32834541-5",
							"kind": "1",
							"name": "Доступ к системе информационно-справочного обслуживания",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "532188c3-d",
							"kind": "1",
							"name": " Возможности бесплатного круглосуточного вызова экстренных оперативных служб",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "dbad0b1a-c",
							"kind": "1",
							"name": "Доступ к услугам внутризоновой, междугородной и международной телефонной связи",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "937ac388-4",
							"kind": "2",
							"name": "Ожидание вызова",
							"type": "1",
							"actual": "true",
							"values": {
								"value": [{
										"qualityDescription": "Нет"
									}, {
										"qualityDescription": "Да"
									}
								]
							},
							"choiceType": "2",
							"isRequired": "false"
						}, {
							"code": "136246bd-9",
							"kind": "2",
							"name": "Переадресация",
							"type": "1",
							"actual": "true",
							"values": {
								"value": [{
										"qualityDescription": "Нет"
									}, {
										"qualityDescription": "Да"
									}
								]
							},
							"choiceType": "2",
							"isRequired": "false"
						}, {
							"code": "c6faf83c-7",
							"kind": "2",
							"name": "Автоматическое определение номера",
							"type": "1",
							"actual": "true",
							"values": {
								"value": [{
										"qualityDescription": "Нет"
									}, {
										"qualityDescription": "Да"
									}
								]
							},
							"choiceType": "2",
							"isRequired": "false"
						}, {
							"code": "f47e41a8-b",
							"kind": "2",
							"name": "Конференц-связь",
							"type": "1",
							"actual": "true",
							"values": {
								"value": [{
										"qualityDescription": "Нет"
									}, {
										"qualityDescription": "Да"
									}
								]
							},
							"choiceType": "2",
							"isRequired": "false"
						}, {
							"code": "fcf8d951-9",
							"kind": "2",
							"name": "Обратный вызов",
							"type": "1",
							"actual": "true",
							"values": {
								"value": [{
										"qualityDescription": "Нет"
									}, {
										"qualityDescription": "Да"
									}
								]
							},
							"choiceType": "2",
							"isRequired": "false"
						}, {
							"code": "c041fa18-1",
							"kind": "2",
							"name": "Ограничения вызовов",
							"type": "1",
							"actual": "true",
							"values": {
								"value": [{
										"qualityDescription": "Нет"
									}, {
										"qualityDescription": "Да"
									}
								]
							},
							"choiceType": "2",
							"isRequired": "false"
						}, {
							"code": "0f27885f-d",
							"kind": "2",
							"name": "Голосовая почта",
							"type": "1",
							"actual": "true",
							"values": {
								"value": [{
										"qualityDescription": "Нет"
									}, {
										"qualityDescription": "Да"
									}
								]
							},
							"choiceType": "2",
							"isRequired": "false"
						}, {
							"code": "4bfe1818-2",
							"kind": "2",
							"name": "Голосовое меню",
							"type": "1",
							"actual": "true",
							"values": {
								"value": [{
										"qualityDescription": "Нет"
									}, {
										"qualityDescription": "Да"
									}
								]
							},
							"choiceType": "2",
							"isRequired": "false"
						}, {
							"code": "3d2cf408-5",
							"kind": "2",
							"name": "Многоканальный номер",
							"type": "1",
							"actual": "true",
							"values": {
								"value": [{
										"qualityDescription": "Нет"
									}, {
										"qualityDescription": "Да"
									}
								]
							},
							"choiceType": "2",
							"isRequired": "false"
						}, {
							"code": "bf0b9c4c-2",
							"kind": "1",
							"name": "В зависимости от набранного номера в сети осуществляются операции по установлению одного из вариантов местного телефонного соединения",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": " С точкой присоединения к местной сети другого оператора (если местное соединение требуется осуществить через сети двух или более операторов)"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "81a6395e-4",
							"kind": "1",
							"name": "Вид тарификации",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Абонентская система оплаты"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}
					]
				},
				"parentPositionInfo": {
					"code": "61.10.11.120-00000001",
					"version": "1"
				},
				"applicationDateStart": "2018-11-20T00:00:00+03:00"
			}
		}
	}, {
		"id": 113041,
		"code": "61.10.11.120-00000008",
		"name": "Услуги местной телефонной связи",
		"data": {
			"data": {
				"NSI": {
					"classifiers": {
						"classifier": {
							"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
							"values": {
								"value": {
									"code": "61.10.11.120",
									"name": "Услуги по предоставлению местных соединений"
								}
							}
						}
					}
				},
				"code": "61.10.11.120-00000008",
				"name": "Услуги местной телефонной связи",
				"OKEIs": {
					"OKEI": {
						"code": "876",
						"name": "Условная единица"
					}
				},
				"OKPD2": {
					"code": "61.10.11.120",
					"name": "Услуги по предоставлению местных соединений"
				},
				"actual": "true",
				"version": "1",
				"isTemplate": "false",
				"publishDate": "2018-10-11T15:48:30+04:00",
				"inclusionDate": "2018-10-11T15:48:30+04:00",
				"characteristics": {
					"characteristic": [{
							"code": "e4eb2ceb-6",
							"kind": "1",
							"name": "Доступ к сети связи исполнителя (оператора)",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "49dc7f3e-a",
							"kind": "1",
							"name": "Абонентская линия в постоянное пользование",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "2aed1d9f-e",
							"kind": "1",
							"name": "Местные телефонные соединения по сети фиксированной телефонной связи с использованием пользовательского (оконечного) оборудования с выделением абоненту номера (номеров) из плана нумерации сети местной телефонной связи для передачи голосовой информации, факсимильных сообщений и передачи данных",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "37c56ad7-b",
							"kind": "1",
							"name": "Доступ к услугам связи сети связи общего пользования",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "32834541-5",
							"kind": "1",
							"name": "Доступ к системе информационно-справочного обслуживания",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "532188c3-d",
							"kind": "1",
							"name": " Возможности бесплатного круглосуточного вызова экстренных оперативных служб",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "dbad0b1a-c",
							"kind": "1",
							"name": "Доступ к услугам внутризоновой, междугородной и международной телефонной связи",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "937ac388-4",
							"kind": "2",
							"name": "Ожидание вызова",
							"type": "1",
							"actual": "true",
							"values": {
								"value": [{
										"qualityDescription": "Нет"
									}, {
										"qualityDescription": "Да"
									}
								]
							},
							"choiceType": "2",
							"isRequired": "false"
						}, {
							"code": "136246bd-9",
							"kind": "2",
							"name": "Переадресация",
							"type": "1",
							"actual": "true",
							"values": {
								"value": [{
										"qualityDescription": "Нет"
									}, {
										"qualityDescription": "Да"
									}
								]
							},
							"choiceType": "2",
							"isRequired": "false"
						}, {
							"code": "c6faf83c-7",
							"kind": "2",
							"name": "Автоматическое определение номера",
							"type": "1",
							"actual": "true",
							"values": {
								"value": [{
										"qualityDescription": "Нет"
									}, {
										"qualityDescription": "Да"
									}
								]
							},
							"choiceType": "2",
							"isRequired": "false"
						}, {
							"code": "f47e41a8-b",
							"kind": "2",
							"name": "Конференц-связь",
							"type": "1",
							"actual": "true",
							"values": {
								"value": [{
										"qualityDescription": "Нет"
									}, {
										"qualityDescription": "Да"
									}
								]
							},
							"choiceType": "2",
							"isRequired": "false"
						}, {
							"code": "fcf8d951-9",
							"kind": "2",
							"name": "Обратный вызов",
							"type": "1",
							"actual": "true",
							"values": {
								"value": [{
										"qualityDescription": "Нет"
									}, {
										"qualityDescription": "Да"
									}
								]
							},
							"choiceType": "2",
							"isRequired": "false"
						}, {
							"code": "c041fa18-1",
							"kind": "2",
							"name": "Ограничения вызовов",
							"type": "1",
							"actual": "true",
							"values": {
								"value": [{
										"qualityDescription": "Нет"
									}, {
										"qualityDescription": "Да"
									}
								]
							},
							"choiceType": "2",
							"isRequired": "false"
						}, {
							"code": "0f27885f-d",
							"kind": "2",
							"name": "Голосовая почта",
							"type": "1",
							"actual": "true",
							"values": {
								"value": [{
										"qualityDescription": "Нет"
									}, {
										"qualityDescription": "Да"
									}
								]
							},
							"choiceType": "2",
							"isRequired": "false"
						}, {
							"code": "4bfe1818-2",
							"kind": "2",
							"name": "Голосовое меню",
							"type": "1",
							"actual": "true",
							"values": {
								"value": [{
										"qualityDescription": "Нет"
									}, {
										"qualityDescription": "Да"
									}
								]
							},
							"choiceType": "2",
							"isRequired": "false"
						}, {
							"code": "3d2cf408-5",
							"kind": "2",
							"name": "Многоканальный номер",
							"type": "1",
							"actual": "true",
							"values": {
								"value": [{
										"qualityDescription": "Нет"
									}, {
										"qualityDescription": "Да"
									}
								]
							},
							"choiceType": "2",
							"isRequired": "false"
						}, {
							"code": "bf0b9c4c-2",
							"kind": "1",
							"name": "В зависимости от набранного номера в сети осуществляются операции по установлению одного из вариантов местного телефонного соединения",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": " С оконечным устройством вызываемого абонента (службы) в пределах местной сети единственного оператора"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "81a6395e-4",
							"kind": "1",
							"name": "Вид тарификации",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Комбинированная система оплаты"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}
					]
				},
				"parentPositionInfo": {
					"code": "61.10.11.120-00000001",
					"version": "1"
				},
				"applicationDateStart": "2018-11-20T00:00:00+03:00"
			}
		}
	}, {
		"id": 113034,
		"code": "61.10.11.120-00000009",
		"name": "Услуги местной телефонной связи",
		"data": {
			"data": {
				"NSI": {
					"classifiers": {
						"classifier": {
							"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
							"values": {
								"value": {
									"code": "61.10.11.120",
									"name": "Услуги по предоставлению местных соединений"
								}
							}
						}
					}
				},
				"code": "61.10.11.120-00000009",
				"name": "Услуги местной телефонной связи",
				"OKEIs": {
					"OKEI": {
						"code": "876",
						"name": "Условная единица"
					}
				},
				"OKPD2": {
					"code": "61.10.11.120",
					"name": "Услуги по предоставлению местных соединений"
				},
				"actual": "true",
				"version": "1",
				"isTemplate": "false",
				"publishDate": "2018-10-11T15:48:31+04:00",
				"inclusionDate": "2018-10-11T15:48:31+04:00",
				"characteristics": {
					"characteristic": [{
							"code": "e4eb2ceb-6",
							"kind": "1",
							"name": "Доступ к сети связи исполнителя (оператора)",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "49dc7f3e-a",
							"kind": "1",
							"name": "Абонентская линия в постоянное пользование",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "2aed1d9f-e",
							"kind": "1",
							"name": "Местные телефонные соединения по сети фиксированной телефонной связи с использованием пользовательского (оконечного) оборудования с выделением абоненту номера (номеров) из плана нумерации сети местной телефонной связи для передачи голосовой информации, факсимильных сообщений и передачи данных",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "37c56ad7-b",
							"kind": "1",
							"name": "Доступ к услугам связи сети связи общего пользования",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "32834541-5",
							"kind": "1",
							"name": "Доступ к системе информационно-справочного обслуживания",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "532188c3-d",
							"kind": "1",
							"name": " Возможности бесплатного круглосуточного вызова экстренных оперативных служб",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "dbad0b1a-c",
							"kind": "1",
							"name": "Доступ к услугам внутризоновой, междугородной и международной телефонной связи",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "937ac388-4",
							"kind": "2",
							"name": "Ожидание вызова",
							"type": "1",
							"actual": "true",
							"values": {
								"value": [{
										"qualityDescription": "Нет"
									}, {
										"qualityDescription": "Да"
									}
								]
							},
							"choiceType": "2",
							"isRequired": "false"
						}, {
							"code": "136246bd-9",
							"kind": "2",
							"name": "Переадресация",
							"type": "1",
							"actual": "true",
							"values": {
								"value": [{
										"qualityDescription": "Нет"
									}, {
										"qualityDescription": "Да"
									}
								]
							},
							"choiceType": "2",
							"isRequired": "false"
						}, {
							"code": "c6faf83c-7",
							"kind": "2",
							"name": "Автоматическое определение номера",
							"type": "1",
							"actual": "true",
							"values": {
								"value": [{
										"qualityDescription": "Нет"
									}, {
										"qualityDescription": "Да"
									}
								]
							},
							"choiceType": "2",
							"isRequired": "false"
						}, {
							"code": "f47e41a8-b",
							"kind": "2",
							"name": "Конференц-связь",
							"type": "1",
							"actual": "true",
							"values": {
								"value": [{
										"qualityDescription": "Нет"
									}, {
										"qualityDescription": "Да"
									}
								]
							},
							"choiceType": "2",
							"isRequired": "false"
						}, {
							"code": "fcf8d951-9",
							"kind": "2",
							"name": "Обратный вызов",
							"type": "1",
							"actual": "true",
							"values": {
								"value": [{
										"qualityDescription": "Нет"
									}, {
										"qualityDescription": "Да"
									}
								]
							},
							"choiceType": "2",
							"isRequired": "false"
						}, {
							"code": "c041fa18-1",
							"kind": "2",
							"name": "Ограничения вызовов",
							"type": "1",
							"actual": "true",
							"values": {
								"value": [{
										"qualityDescription": "Нет"
									}, {
										"qualityDescription": "Да"
									}
								]
							},
							"choiceType": "2",
							"isRequired": "false"
						}, {
							"code": "0f27885f-d",
							"kind": "2",
							"name": "Голосовая почта",
							"type": "1",
							"actual": "true",
							"values": {
								"value": [{
										"qualityDescription": "Нет"
									}, {
										"qualityDescription": "Да"
									}
								]
							},
							"choiceType": "2",
							"isRequired": "false"
						}, {
							"code": "4bfe1818-2",
							"kind": "2",
							"name": "Голосовое меню",
							"type": "1",
							"actual": "true",
							"values": {
								"value": [{
										"qualityDescription": "Нет"
									}, {
										"qualityDescription": "Да"
									}
								]
							},
							"choiceType": "2",
							"isRequired": "false"
						}, {
							"code": "3d2cf408-5",
							"kind": "2",
							"name": "Многоканальный номер",
							"type": "1",
							"actual": "true",
							"values": {
								"value": [{
										"qualityDescription": "Нет"
									}, {
										"qualityDescription": "Да"
									}
								]
							},
							"choiceType": "2",
							"isRequired": "false"
						}, {
							"code": "bf0b9c4c-2",
							"kind": "1",
							"name": "В зависимости от набранного номера в сети осуществляются операции по установлению одного из вариантов местного телефонного соединения",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": " С оконечным устройством вызываемого абонента (службы) в пределах местной сети единственного оператора"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "81a6395e-4",
							"kind": "1",
							"name": "Вид тарификации",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Повременная система оплаты"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}
					]
				},
				"parentPositionInfo": {
					"code": "61.10.11.120-00000001",
					"version": "1"
				},
				"applicationDateStart": "2018-11-20T00:00:00+03:00"
			}
		}
	}, {
		"id": 113033,
		"code": "61.10.11.120-00000010",
		"name": "Услуги местной телефонной связи",
		"data": {
			"data": {
				"NSI": {
					"classifiers": {
						"classifier": {
							"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
							"values": {
								"value": {
									"code": "61.10.11.120",
									"name": "Услуги по предоставлению местных соединений"
								}
							}
						}
					}
				},
				"code": "61.10.11.120-00000010",
				"name": "Услуги местной телефонной связи",
				"OKEIs": {
					"OKEI": {
						"code": "876",
						"name": "Условная единица"
					}
				},
				"OKPD2": {
					"code": "61.10.11.120",
					"name": "Услуги по предоставлению местных соединений"
				},
				"actual": "true",
				"version": "1",
				"isTemplate": "false",
				"publishDate": "2018-10-11T15:48:31+04:00",
				"inclusionDate": "2018-10-11T15:48:31+04:00",
				"characteristics": {
					"characteristic": [{
							"code": "e4eb2ceb-6",
							"kind": "1",
							"name": "Доступ к сети связи исполнителя (оператора)",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "49dc7f3e-a",
							"kind": "1",
							"name": "Абонентская линия в постоянное пользование",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "2aed1d9f-e",
							"kind": "1",
							"name": "Местные телефонные соединения по сети фиксированной телефонной связи с использованием пользовательского (оконечного) оборудования с выделением абоненту номера (номеров) из плана нумерации сети местной телефонной связи для передачи голосовой информации, факсимильных сообщений и передачи данных",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "37c56ad7-b",
							"kind": "1",
							"name": "Доступ к услугам связи сети связи общего пользования",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "32834541-5",
							"kind": "1",
							"name": "Доступ к системе информационно-справочного обслуживания",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "532188c3-d",
							"kind": "1",
							"name": " Возможности бесплатного круглосуточного вызова экстренных оперативных служб",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "dbad0b1a-c",
							"kind": "1",
							"name": "Доступ к услугам внутризоновой, междугородной и международной телефонной связи",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Да"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "937ac388-4",
							"kind": "2",
							"name": "Ожидание вызова",
							"type": "1",
							"actual": "true",
							"values": {
								"value": [{
										"qualityDescription": "Нет"
									}, {
										"qualityDescription": "Да"
									}
								]
							},
							"choiceType": "2",
							"isRequired": "false"
						}, {
							"code": "136246bd-9",
							"kind": "2",
							"name": "Переадресация",
							"type": "1",
							"actual": "true",
							"values": {
								"value": [{
										"qualityDescription": "Нет"
									}, {
										"qualityDescription": "Да"
									}
								]
							},
							"choiceType": "2",
							"isRequired": "false"
						}, {
							"code": "c6faf83c-7",
							"kind": "2",
							"name": "Автоматическое определение номера",
							"type": "1",
							"actual": "true",
							"values": {
								"value": [{
										"qualityDescription": "Нет"
									}, {
										"qualityDescription": "Да"
									}
								]
							},
							"choiceType": "2",
							"isRequired": "false"
						}, {
							"code": "f47e41a8-b",
							"kind": "2",
							"name": "Конференц-связь",
							"type": "1",
							"actual": "true",
							"values": {
								"value": [{
										"qualityDescription": "Нет"
									}, {
										"qualityDescription": "Да"
									}
								]
							},
							"choiceType": "2",
							"isRequired": "false"
						}, {
							"code": "fcf8d951-9",
							"kind": "2",
							"name": "Обратный вызов",
							"type": "1",
							"actual": "true",
							"values": {
								"value": [{
										"qualityDescription": "Нет"
									}, {
										"qualityDescription": "Да"
									}
								]
							},
							"choiceType": "2",
							"isRequired": "false"
						}, {
							"code": "c041fa18-1",
							"kind": "2",
							"name": "Ограничения вызовов",
							"type": "1",
							"actual": "true",
							"values": {
								"value": [{
										"qualityDescription": "Нет"
									}, {
										"qualityDescription": "Да"
									}
								]
							},
							"choiceType": "2",
							"isRequired": "false"
						}, {
							"code": "0f27885f-d",
							"kind": "2",
							"name": "Голосовая почта",
							"type": "1",
							"actual": "true",
							"values": {
								"value": [{
										"qualityDescription": "Нет"
									}, {
										"qualityDescription": "Да"
									}
								]
							},
							"choiceType": "2",
							"isRequired": "false"
						}, {
							"code": "4bfe1818-2",
							"kind": "2",
							"name": "Голосовое меню",
							"type": "1",
							"actual": "true",
							"values": {
								"value": [{
										"qualityDescription": "Нет"
									}, {
										"qualityDescription": "Да"
									}
								]
							},
							"choiceType": "2",
							"isRequired": "false"
						}, {
							"code": "3d2cf408-5",
							"kind": "2",
							"name": "Многоканальный номер",
							"type": "1",
							"actual": "true",
							"values": {
								"value": [{
										"qualityDescription": "Нет"
									}, {
										"qualityDescription": "Да"
									}
								]
							},
							"choiceType": "2",
							"isRequired": "false"
						}, {
							"code": "bf0b9c4c-2",
							"kind": "1",
							"name": "В зависимости от набранного номера в сети осуществляются операции по установлению одного из вариантов местного телефонного соединения",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": " С оконечным устройством вызываемого абонента (службы) в пределах местной сети единственного оператора"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}, {
							"code": "81a6395e-4",
							"kind": "1",
							"name": "Вид тарификации",
							"type": "1",
							"actual": "true",
							"values": {
								"value": {
									"qualityDescription": "Абонентская система оплаты"
								}
							},
							"choiceType": "1",
							"isRequired": "true"
						}
					]
				},
				"parentPositionInfo": {
					"code": "61.10.11.120-00000001",
					"version": "1"
				},
				"applicationDateStart": "2018-11-20T00:00:00+03:00"
			}
		}
	}, {
		"id": 49561,
		"code": "61.10.20.000-00000001",
		"name": "Услуги операторов связи в сфере проводных телекоммуникаций",
		"data": {
			"data": {
				"NSI": {
					"classifiers": {
						"classifier": {
							"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
							"values": {
								"value": {
									"code": "61.10.2",
									"name": "Услуги операторов связи в сфере проводных телекоммуникаций"
								}
							}
						}
					}
				},
				"code": "61.10.20.000-00000001",
				"name": "Услуги операторов связи в сфере проводных телекоммуникаций",
				"OKEIs": {
					"OKEI": {
						"code": "257",
						"name": "Мегабайт"
					}
				},
				"OKPD2": {
					"code": "61.10.2",
					"name": "Услуги операторов связи в сфере проводных телекоммуникаций"
				},
				"actual": "true",
				"version": "1",
				"isTemplate": "false",
				"publishDate": "2018-09-04T22:11:15+04:00",
				"inclusionDate": "2018-09-04T22:11:15+04:00",
				"applicationDateStart": "2019-07-01T00:00:00+04:00"
			}
		}
	}, {
		"id": 49558,
		"code": "61.10.20.000-00000002",
		"name": "Услуги операторов связи в сфере проводных телекоммуникаций",
		"data": {
			"data": {
				"NSI": {
					"classifiers": {
						"classifier": {
							"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
							"values": {
								"value": {
									"code": "61.10.2",
									"name": "Услуги операторов связи в сфере проводных телекоммуникаций"
								}
							}
						}
					}
				},
				"code": "61.10.20.000-00000002",
				"name": "Услуги операторов связи в сфере проводных телекоммуникаций",
				"OKEIs": {
					"OKEI": {
						"code": "359",
						"name": "Сутки"
					}
				},
				"OKPD2": {
					"code": "61.10.2",
					"name": "Услуги операторов связи в сфере проводных телекоммуникаций"
				},
				"actual": "true",
				"version": "1",
				"isTemplate": "false",
				"publishDate": "2018-09-04T22:11:17+04:00",
				"inclusionDate": "2018-09-04T22:11:17+04:00",
				"applicationDateStart": "2019-07-01T00:00:00+04:00"
			}
		}
	}, {
		"id": 49554,
		"code": "61.10.20.000-00000003",
		"name": "Услуги операторов связи в сфере проводных телекоммуникаций",
		"data": {
			"data": {
				"NSI": {
					"classifiers": {
						"classifier": {
							"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
							"values": {
								"value": {
									"code": "61.10.2",
									"name": "Услуги операторов связи в сфере проводных телекоммуникаций"
								}
							}
						}
					}
				},
				"code": "61.10.20.000-00000003",
				"name": "Услуги операторов связи в сфере проводных телекоммуникаций",
				"OKEIs": {
					"OKEI": {
						"code": "362",
						"name": "Месяц"
					}
				},
				"OKPD2": {
					"code": "61.10.2",
					"name": "Услуги операторов связи в сфере проводных телекоммуникаций"
				},
				"actual": "true",
				"version": "1",
				"isTemplate": "false",
				"publishDate": "2018-09-04T22:11:19+04:00",
				"inclusionDate": "2018-09-04T22:11:19+04:00",
				"applicationDateStart": "2019-07-01T00:00:00+04:00"
			}
		}
	}, {
		"id": 49551,
		"code": "61.10.20.000-00000004",
		"name": "Услуги операторов связи в сфере проводных телекоммуникаций",
		"data": {
			"data": {
				"NSI": {
					"classifiers": {
						"classifier": {
							"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
							"values": {
								"value": {
									"code": "61.10.2",
									"name": "Услуги операторов связи в сфере проводных телекоммуникаций"
								}
							}
						}
					}
				},
				"code": "61.10.20.000-00000004",
				"name": "Услуги операторов связи в сфере проводных телекоммуникаций",
				"OKEIs": {
					"OKEI": {
						"code": "876",
						"name": "Условная единица"
					}
				},
				"OKPD2": {
					"code": "61.10.2",
					"name": "Услуги операторов связи в сфере проводных телекоммуникаций"
				},
				"actual": "true",
				"version": "1",
				"isTemplate": "false",
				"publishDate": "2018-09-04T22:11:21+04:00",
				"inclusionDate": "2018-09-04T22:11:21+04:00",
				"applicationDateStart": "2019-07-01T00:00:00+04:00"
			}
		}
	}, {
		"id": 112816,
		"code": "61.10.30.190-00000001",
		"name": "Услуги по предоставлению выделенного цифрового канала с временным разделением (TDM)",
		"data": {
			"data": {
				"NSI": {
					"classifiers": {
						"classifier": [{
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.10.30.190",
										"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
									}
								}
							}, {
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.20.30.120",
										"name": "Услуги по передаче данных по беспроводным телекоммуникационным сетям"
									}
								}
							}
						]
					}
				},
				"code": "61.10.30.190-00000001",
				"name": "Услуги по предоставлению выделенного цифрового канала с временным разделением (TDM)",
				"OKEIs": {
					"OKEI": {
						"code": "876",
						"name": "Условная единица"
					}
				},
				"OKPD2": {
					"code": "61.10.30.190",
					"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
				},
				"actual": "true",
				"version": "1",
				"isTemplate": "true",
				"publishDate": "2018-10-11T17:29:55+04:00",
				"inclusionDate": "2018-10-11T17:29:55+04:00",
				"characteristics": {
					"characteristic": {
						"code": "ee420243-9",
						"kind": "1",
						"name": "Пропускная способность",
						"type": "2",
						"actual": "true",
						"values": {
							"value": [{
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "622",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "155",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "34",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "2",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "1",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": ".512",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": ".256",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": ".128",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": ".064",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}
							]
						},
						"choiceType": "1",
						"isRequired": "true"
					}
				},
				"applicationDateStart": "2018-11-20T00:00:00+03:00"
			}
		}
	}, {
		"id": 112825,
		"code": "61.10.30.190-00000002",
		"name": "Услуги по предоставлению выделенного цифрового канала с временным разделением (TDM)",
		"data": {
			"data": {
				"NSI": {
					"classifiers": {
						"classifier": [{
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.10.30.190",
										"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
									}
								}
							}, {
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.20.30.120",
										"name": "Услуги по передаче данных по беспроводным телекоммуникационным сетям"
									}
								}
							}
						]
					}
				},
				"code": "61.10.30.190-00000002",
				"name": "Услуги по предоставлению выделенного цифрового канала с временным разделением (TDM)",
				"OKEIs": {
					"OKEI": {
						"code": "876",
						"name": "Условная единица"
					}
				},
				"OKPD2": {
					"code": "61.10.30.190",
					"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
				},
				"actual": "true",
				"version": "1",
				"isTemplate": "false",
				"publishDate": "2018-10-11T17:29:56+04:00",
				"inclusionDate": "2018-10-11T17:29:56+04:00",
				"characteristics": {
					"characteristic": {
						"code": "ee420243-9",
						"kind": "1",
						"name": "Пропускная способность",
						"type": "2",
						"actual": "true",
						"values": {
							"value": {
								"OKEI": {
									"code": "2545",
									"name": "Мегабит в секунду"
								},
								"rangeSet": {
									"valueRange": {
										"min": "622",
										"minMathNotation": "greaterOrEqual"
									}
								},
								"valueFormat": "A"
							}
						},
						"choiceType": "1",
						"isRequired": "true"
					}
				},
				"parentPositionInfo": {
					"code": "61.10.30.190-00000001",
					"version": "1"
				},
				"applicationDateStart": "2018-11-20T00:00:00+03:00"
			}
		}
	}, {
		"id": 112824,
		"code": "61.10.30.190-00000003",
		"name": "Услуги по предоставлению выделенного цифрового канала с временным разделением (TDM)",
		"data": {
			"data": {
				"NSI": {
					"classifiers": {
						"classifier": [{
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.10.30.190",
										"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
									}
								}
							}, {
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.20.30.120",
										"name": "Услуги по передаче данных по беспроводным телекоммуникационным сетям"
									}
								}
							}
						]
					}
				},
				"code": "61.10.30.190-00000003",
				"name": "Услуги по предоставлению выделенного цифрового канала с временным разделением (TDM)",
				"OKEIs": {
					"OKEI": {
						"code": "876",
						"name": "Условная единица"
					}
				},
				"OKPD2": {
					"code": "61.10.30.190",
					"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
				},
				"actual": "true",
				"version": "1",
				"isTemplate": "false",
				"publishDate": "2018-10-11T17:29:56+04:00",
				"inclusionDate": "2018-10-11T17:29:56+04:00",
				"characteristics": {
					"characteristic": {
						"code": "ee420243-9",
						"kind": "1",
						"name": "Пропускная способность",
						"type": "2",
						"actual": "true",
						"values": {
							"value": {
								"OKEI": {
									"code": "2545",
									"name": "Мегабит в секунду"
								},
								"rangeSet": {
									"valueRange": {
										"min": "155",
										"minMathNotation": "greaterOrEqual"
									}
								},
								"valueFormat": "A"
							}
						},
						"choiceType": "1",
						"isRequired": "true"
					}
				},
				"parentPositionInfo": {
					"code": "61.10.30.190-00000001",
					"version": "1"
				},
				"applicationDateStart": "2018-11-20T00:00:00+03:00"
			}
		}
	}, {
		"id": 112823,
		"code": "61.10.30.190-00000004",
		"name": "Услуги по предоставлению выделенного цифрового канала с временным разделением (TDM)",
		"data": {
			"data": {
				"NSI": {
					"classifiers": {
						"classifier": [{
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.10.30.190",
										"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
									}
								}
							}, {
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.20.30.120",
										"name": "Услуги по передаче данных по беспроводным телекоммуникационным сетям"
									}
								}
							}
						]
					}
				},
				"code": "61.10.30.190-00000004",
				"name": "Услуги по предоставлению выделенного цифрового канала с временным разделением (TDM)",
				"OKEIs": {
					"OKEI": {
						"code": "876",
						"name": "Условная единица"
					}
				},
				"OKPD2": {
					"code": "61.10.30.190",
					"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
				},
				"actual": "true",
				"version": "1",
				"isTemplate": "false",
				"publishDate": "2018-10-11T17:29:56+04:00",
				"inclusionDate": "2018-10-11T17:29:56+04:00",
				"characteristics": {
					"characteristic": {
						"code": "ee420243-9",
						"kind": "1",
						"name": "Пропускная способность",
						"type": "2",
						"actual": "true",
						"values": {
							"value": {
								"OKEI": {
									"code": "2545",
									"name": "Мегабит в секунду"
								},
								"rangeSet": {
									"valueRange": {
										"min": "34",
										"minMathNotation": "greaterOrEqual"
									}
								},
								"valueFormat": "A"
							}
						},
						"choiceType": "1",
						"isRequired": "true"
					}
				},
				"parentPositionInfo": {
					"code": "61.10.30.190-00000001",
					"version": "1"
				},
				"applicationDateStart": "2018-11-20T00:00:00+03:00"
			}
		}
	}, {
		"id": 112822,
		"code": "61.10.30.190-00000005",
		"name": "Услуги по предоставлению выделенного цифрового канала с временным разделением (TDM)",
		"data": {
			"data": {
				"NSI": {
					"classifiers": {
						"classifier": [{
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.10.30.190",
										"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
									}
								}
							}, {
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.20.30.120",
										"name": "Услуги по передаче данных по беспроводным телекоммуникационным сетям"
									}
								}
							}
						]
					}
				},
				"code": "61.10.30.190-00000005",
				"name": "Услуги по предоставлению выделенного цифрового канала с временным разделением (TDM)",
				"OKEIs": {
					"OKEI": {
						"code": "876",
						"name": "Условная единица"
					}
				},
				"OKPD2": {
					"code": "61.10.30.190",
					"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
				},
				"actual": "true",
				"version": "1",
				"isTemplate": "false",
				"publishDate": "2018-10-11T17:29:57+04:00",
				"inclusionDate": "2018-10-11T17:29:57+04:00",
				"characteristics": {
					"characteristic": {
						"code": "ee420243-9",
						"kind": "1",
						"name": "Пропускная способность",
						"type": "2",
						"actual": "true",
						"values": {
							"value": {
								"OKEI": {
									"code": "2545",
									"name": "Мегабит в секунду"
								},
								"rangeSet": {
									"valueRange": {
										"min": "2",
										"minMathNotation": "greaterOrEqual"
									}
								},
								"valueFormat": "A"
							}
						},
						"choiceType": "1",
						"isRequired": "true"
					}
				},
				"parentPositionInfo": {
					"code": "61.10.30.190-00000001",
					"version": "1"
				},
				"applicationDateStart": "2018-11-20T00:00:00+03:00"
			}
		}
	}, {
		"id": 112821,
		"code": "61.10.30.190-00000006",
		"name": "Услуги по предоставлению выделенного цифрового канала с временным разделением (TDM)",
		"data": {
			"data": {
				"NSI": {
					"classifiers": {
						"classifier": [{
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.10.30.190",
										"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
									}
								}
							}, {
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.20.30.120",
										"name": "Услуги по передаче данных по беспроводным телекоммуникационным сетям"
									}
								}
							}
						]
					}
				},
				"code": "61.10.30.190-00000006",
				"name": "Услуги по предоставлению выделенного цифрового канала с временным разделением (TDM)",
				"OKEIs": {
					"OKEI": {
						"code": "876",
						"name": "Условная единица"
					}
				},
				"OKPD2": {
					"code": "61.10.30.190",
					"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
				},
				"actual": "true",
				"version": "1",
				"isTemplate": "false",
				"publishDate": "2018-10-11T17:29:57+04:00",
				"inclusionDate": "2018-10-11T17:29:57+04:00",
				"characteristics": {
					"characteristic": {
						"code": "ee420243-9",
						"kind": "1",
						"name": "Пропускная способность",
						"type": "2",
						"actual": "true",
						"values": {
							"value": {
								"OKEI": {
									"code": "2545",
									"name": "Мегабит в секунду"
								},
								"rangeSet": {
									"valueRange": {
										"min": "1",
										"minMathNotation": "greaterOrEqual"
									}
								},
								"valueFormat": "A"
							}
						},
						"choiceType": "1",
						"isRequired": "true"
					}
				},
				"parentPositionInfo": {
					"code": "61.10.30.190-00000001",
					"version": "1"
				},
				"applicationDateStart": "2018-11-20T00:00:00+03:00"
			}
		}
	}, {
		"id": 112820,
		"code": "61.10.30.190-00000007",
		"name": "Услуги по предоставлению выделенного цифрового канала с временным разделением (TDM)",
		"data": {
			"data": {
				"NSI": {
					"classifiers": {
						"classifier": [{
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.10.30.190",
										"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
									}
								}
							}, {
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.20.30.120",
										"name": "Услуги по передаче данных по беспроводным телекоммуникационным сетям"
									}
								}
							}
						]
					}
				},
				"code": "61.10.30.190-00000007",
				"name": "Услуги по предоставлению выделенного цифрового канала с временным разделением (TDM)",
				"OKEIs": {
					"OKEI": {
						"code": "876",
						"name": "Условная единица"
					}
				},
				"OKPD2": {
					"code": "61.10.30.190",
					"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
				},
				"actual": "true",
				"version": "1",
				"isTemplate": "false",
				"publishDate": "2018-10-11T17:29:58+04:00",
				"inclusionDate": "2018-10-11T17:29:58+04:00",
				"characteristics": {
					"characteristic": {
						"code": "ee420243-9",
						"kind": "1",
						"name": "Пропускная способность",
						"type": "2",
						"actual": "true",
						"values": {
							"value": {
								"OKEI": {
									"code": "2545",
									"name": "Мегабит в секунду"
								},
								"rangeSet": {
									"valueRange": {
										"min": ".512",
										"minMathNotation": "greaterOrEqual"
									}
								},
								"valueFormat": "A"
							}
						},
						"choiceType": "1",
						"isRequired": "true"
					}
				},
				"parentPositionInfo": {
					"code": "61.10.30.190-00000001",
					"version": "1"
				},
				"applicationDateStart": "2018-11-20T00:00:00+03:00"
			}
		}
	}, {
		"id": 112819,
		"code": "61.10.30.190-00000008",
		"name": "Услуги по предоставлению выделенного цифрового канала с временным разделением (TDM)",
		"data": {
			"data": {
				"NSI": {
					"classifiers": {
						"classifier": [{
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.10.30.190",
										"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
									}
								}
							}, {
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.20.30.120",
										"name": "Услуги по передаче данных по беспроводным телекоммуникационным сетям"
									}
								}
							}
						]
					}
				},
				"code": "61.10.30.190-00000008",
				"name": "Услуги по предоставлению выделенного цифрового канала с временным разделением (TDM)",
				"OKEIs": {
					"OKEI": {
						"code": "876",
						"name": "Условная единица"
					}
				},
				"OKPD2": {
					"code": "61.10.30.190",
					"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
				},
				"actual": "true",
				"version": "1",
				"isTemplate": "false",
				"publishDate": "2018-10-11T17:29:58+04:00",
				"inclusionDate": "2018-10-11T17:29:58+04:00",
				"characteristics": {
					"characteristic": {
						"code": "ee420243-9",
						"kind": "1",
						"name": "Пропускная способность",
						"type": "2",
						"actual": "true",
						"values": {
							"value": {
								"OKEI": {
									"code": "2545",
									"name": "Мегабит в секунду"
								},
								"rangeSet": {
									"valueRange": {
										"min": ".256",
										"minMathNotation": "greaterOrEqual"
									}
								},
								"valueFormat": "A"
							}
						},
						"choiceType": "1",
						"isRequired": "true"
					}
				},
				"parentPositionInfo": {
					"code": "61.10.30.190-00000001",
					"version": "1"
				},
				"applicationDateStart": "2018-11-20T00:00:00+03:00"
			}
		}
	}, {
		"id": 112818,
		"code": "61.10.30.190-00000009",
		"name": "Услуги по предоставлению выделенного цифрового канала с временным разделением (TDM)",
		"data": {
			"data": {
				"NSI": {
					"classifiers": {
						"classifier": [{
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.10.30.190",
										"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
									}
								}
							}, {
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.20.30.120",
										"name": "Услуги по передаче данных по беспроводным телекоммуникационным сетям"
									}
								}
							}
						]
					}
				},
				"code": "61.10.30.190-00000009",
				"name": "Услуги по предоставлению выделенного цифрового канала с временным разделением (TDM)",
				"OKEIs": {
					"OKEI": {
						"code": "876",
						"name": "Условная единица"
					}
				},
				"OKPD2": {
					"code": "61.10.30.190",
					"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
				},
				"actual": "true",
				"version": "1",
				"isTemplate": "false",
				"publishDate": "2018-10-11T17:29:59+04:00",
				"inclusionDate": "2018-10-11T17:29:59+04:00",
				"characteristics": {
					"characteristic": {
						"code": "ee420243-9",
						"kind": "1",
						"name": "Пропускная способность",
						"type": "2",
						"actual": "true",
						"values": {
							"value": {
								"OKEI": {
									"code": "2545",
									"name": "Мегабит в секунду"
								},
								"rangeSet": {
									"valueRange": {
										"min": ".128",
										"minMathNotation": "greaterOrEqual"
									}
								},
								"valueFormat": "A"
							}
						},
						"choiceType": "1",
						"isRequired": "true"
					}
				},
				"parentPositionInfo": {
					"code": "61.10.30.190-00000001",
					"version": "1"
				},
				"applicationDateStart": "2018-11-20T00:00:00+03:00"
			}
		}
	}, {
		"id": 112817,
		"code": "61.10.30.190-00000010",
		"name": "Услуги по предоставлению выделенного цифрового канала с временным разделением (TDM)",
		"data": {
			"data": {
				"NSI": {
					"classifiers": {
						"classifier": [{
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.10.30.190",
										"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
									}
								}
							}, {
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.20.30.120",
										"name": "Услуги по передаче данных по беспроводным телекоммуникационным сетям"
									}
								}
							}
						]
					}
				},
				"code": "61.10.30.190-00000010",
				"name": "Услуги по предоставлению выделенного цифрового канала с временным разделением (TDM)",
				"OKEIs": {
					"OKEI": {
						"code": "876",
						"name": "Условная единица"
					}
				},
				"OKPD2": {
					"code": "61.10.30.190",
					"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
				},
				"actual": "true",
				"version": "1",
				"isTemplate": "false",
				"publishDate": "2018-10-11T17:29:59+04:00",
				"inclusionDate": "2018-10-11T17:29:59+04:00",
				"characteristics": {
					"characteristic": {
						"code": "ee420243-9",
						"kind": "1",
						"name": "Пропускная способность",
						"type": "2",
						"actual": "true",
						"values": {
							"value": {
								"OKEI": {
									"code": "2545",
									"name": "Мегабит в секунду"
								},
								"rangeSet": {
									"valueRange": {
										"min": ".064",
										"minMathNotation": "greaterOrEqual"
									}
								},
								"valueFormat": "A"
							}
						},
						"choiceType": "1",
						"isRequired": "true"
					}
				},
				"parentPositionInfo": {
					"code": "61.10.30.190-00000001",
					"version": "1"
				},
				"applicationDateStart": "2018-11-20T00:00:00+03:00"
			}
		}
	}, {
		"id": 112888,
		"code": "61.10.30.190-00000011",
		"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
		"data": {
			"data": {
				"NSI": {
					"classifiers": {
						"classifier": [{
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.10.30.190",
										"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
									}
								}
							}, {
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.20.30.120",
										"name": "Услуги по передаче данных по беспроводным телекоммуникационным сетям"
									}
								}
							}
						]
					}
				},
				"code": "61.10.30.190-00000011",
				"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
				"OKEIs": {
					"OKEI": {
						"code": "876",
						"name": "Условная единица"
					}
				},
				"OKPD2": {
					"code": "61.10.30.190",
					"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
				},
				"actual": "true",
				"version": "1",
				"isTemplate": "true",
				"publishDate": "2018-10-11T17:36:38+04:00",
				"inclusionDate": "2018-10-11T17:36:38+04:00",
				"characteristics": {
					"characteristic": {
						"code": "570b2c9f-0",
						"kind": "1",
						"name": "Пропускная способность",
						"type": "2",
						"actual": "true",
						"values": {
							"value": [{
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "100000",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "40000",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "20000",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "10000",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "9000",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "8000",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "7000",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "6000",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "5000",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "4000",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "3000",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "2000",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "1000",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "950",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "900",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "850",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "800",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "750",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "700",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "650",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "600",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "550",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "500",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "450",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "400",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "350",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "300",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "250",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "200",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "190",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "180",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "170",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "160",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "150",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "140",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "130",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "120",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "110",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "100",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "90",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "80",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "70",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "60",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "50",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "40",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "30",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "20",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "10",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "9",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "8",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "7",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "6",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "5",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "4",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "3",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "2",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "1",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": ".512",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": ".256",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": ".128",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": ".064",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": ".016",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}
							]
						},
						"choiceType": "1",
						"isRequired": "true"
					}
				},
				"applicationDateStart": "2018-11-20T00:00:00+03:00"
			}
		}
	}, {
		"id": 112952,
		"code": "61.10.30.190-00000012",
		"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
		"data": {
			"data": {
				"NSI": {
					"classifiers": {
						"classifier": [{
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.10.30.190",
										"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
									}
								}
							}, {
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.20.30.120",
										"name": "Услуги по передаче данных по беспроводным телекоммуникационным сетям"
									}
								}
							}
						]
					}
				},
				"code": "61.10.30.190-00000012",
				"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
				"OKEIs": {
					"OKEI": {
						"code": "876",
						"name": "Условная единица"
					}
				},
				"OKPD2": {
					"code": "61.10.30.190",
					"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
				},
				"actual": "true",
				"version": "1",
				"isTemplate": "false",
				"publishDate": "2018-10-11T17:36:38+04:00",
				"inclusionDate": "2018-10-11T17:36:38+04:00",
				"characteristics": {
					"characteristic": {
						"code": "570b2c9f-0",
						"kind": "1",
						"name": "Пропускная способность",
						"type": "2",
						"actual": "true",
						"values": {
							"value": {
								"OKEI": {
									"code": "2545",
									"name": "Мегабит в секунду"
								},
								"rangeSet": {
									"valueRange": {
										"min": "100000",
										"minMathNotation": "greaterOrEqual"
									}
								},
								"valueFormat": "A"
							}
						},
						"choiceType": "1",
						"isRequired": "true"
					}
				},
				"parentPositionInfo": {
					"code": "61.10.30.190-00000011",
					"version": "1"
				},
				"applicationDateStart": "2018-11-20T00:00:00+03:00"
			}
		}
	}, {
		"id": 112951,
		"code": "61.10.30.190-00000013",
		"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
		"data": {
			"data": {
				"NSI": {
					"classifiers": {
						"classifier": [{
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.10.30.190",
										"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
									}
								}
							}, {
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.20.30.120",
										"name": "Услуги по передаче данных по беспроводным телекоммуникационным сетям"
									}
								}
							}
						]
					}
				},
				"code": "61.10.30.190-00000013",
				"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
				"OKEIs": {
					"OKEI": {
						"code": "876",
						"name": "Условная единица"
					}
				},
				"OKPD2": {
					"code": "61.10.30.190",
					"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
				},
				"actual": "true",
				"version": "1",
				"isTemplate": "false",
				"publishDate": "2018-10-11T17:36:39+04:00",
				"inclusionDate": "2018-10-11T17:36:39+04:00",
				"characteristics": {
					"characteristic": {
						"code": "570b2c9f-0",
						"kind": "1",
						"name": "Пропускная способность",
						"type": "2",
						"actual": "true",
						"values": {
							"value": {
								"OKEI": {
									"code": "2545",
									"name": "Мегабит в секунду"
								},
								"rangeSet": {
									"valueRange": {
										"min": "40000",
										"minMathNotation": "greaterOrEqual"
									}
								},
								"valueFormat": "A"
							}
						},
						"choiceType": "1",
						"isRequired": "true"
					}
				},
				"parentPositionInfo": {
					"code": "61.10.30.190-00000011",
					"version": "1"
				},
				"applicationDateStart": "2018-11-20T00:00:00+03:00"
			}
		}
	}, {
		"id": 112950,
		"code": "61.10.30.190-00000014",
		"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
		"data": {
			"data": {
				"NSI": {
					"classifiers": {
						"classifier": [{
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.10.30.190",
										"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
									}
								}
							}, {
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.20.30.120",
										"name": "Услуги по передаче данных по беспроводным телекоммуникационным сетям"
									}
								}
							}
						]
					}
				},
				"code": "61.10.30.190-00000014",
				"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
				"OKEIs": {
					"OKEI": {
						"code": "876",
						"name": "Условная единица"
					}
				},
				"OKPD2": {
					"code": "61.10.30.190",
					"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
				},
				"actual": "true",
				"version": "1",
				"isTemplate": "false",
				"publishDate": "2018-10-11T17:36:39+04:00",
				"inclusionDate": "2018-10-11T17:36:39+04:00",
				"characteristics": {
					"characteristic": {
						"code": "570b2c9f-0",
						"kind": "1",
						"name": "Пропускная способность",
						"type": "2",
						"actual": "true",
						"values": {
							"value": {
								"OKEI": {
									"code": "2545",
									"name": "Мегабит в секунду"
								},
								"rangeSet": {
									"valueRange": {
										"min": "20000",
										"minMathNotation": "greaterOrEqual"
									}
								},
								"valueFormat": "A"
							}
						},
						"choiceType": "1",
						"isRequired": "true"
					}
				},
				"parentPositionInfo": {
					"code": "61.10.30.190-00000011",
					"version": "1"
				},
				"applicationDateStart": "2018-11-20T00:00:00+03:00"
			}
		}
	}, {
		"id": 112949,
		"code": "61.10.30.190-00000015",
		"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
		"data": {
			"data": {
				"NSI": {
					"classifiers": {
						"classifier": [{
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.10.30.190",
										"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
									}
								}
							}, {
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.20.30.120",
										"name": "Услуги по передаче данных по беспроводным телекоммуникационным сетям"
									}
								}
							}
						]
					}
				},
				"code": "61.10.30.190-00000015",
				"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
				"OKEIs": {
					"OKEI": {
						"code": "876",
						"name": "Условная единица"
					}
				},
				"OKPD2": {
					"code": "61.10.30.190",
					"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
				},
				"actual": "true",
				"version": "1",
				"isTemplate": "false",
				"publishDate": "2018-10-11T17:36:40+04:00",
				"inclusionDate": "2018-10-11T17:36:40+04:00",
				"characteristics": {
					"characteristic": {
						"code": "570b2c9f-0",
						"kind": "1",
						"name": "Пропускная способность",
						"type": "2",
						"actual": "true",
						"values": {
							"value": {
								"OKEI": {
									"code": "2545",
									"name": "Мегабит в секунду"
								},
								"rangeSet": {
									"valueRange": {
										"min": "10000",
										"minMathNotation": "greaterOrEqual"
									}
								},
								"valueFormat": "A"
							}
						},
						"choiceType": "1",
						"isRequired": "true"
					}
				},
				"parentPositionInfo": {
					"code": "61.10.30.190-00000011",
					"version": "1"
				},
				"applicationDateStart": "2018-11-20T00:00:00+03:00"
			}
		}
	}, {
		"id": 112948,
		"code": "61.10.30.190-00000016",
		"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
		"data": {
			"data": {
				"NSI": {
					"classifiers": {
						"classifier": [{
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.10.30.190",
										"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
									}
								}
							}, {
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.20.30.120",
										"name": "Услуги по передаче данных по беспроводным телекоммуникационным сетям"
									}
								}
							}
						]
					}
				},
				"code": "61.10.30.190-00000016",
				"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
				"OKEIs": {
					"OKEI": {
						"code": "876",
						"name": "Условная единица"
					}
				},
				"OKPD2": {
					"code": "61.10.30.190",
					"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
				},
				"actual": "true",
				"version": "1",
				"isTemplate": "false",
				"publishDate": "2018-10-11T17:36:40+04:00",
				"inclusionDate": "2018-10-11T17:36:40+04:00",
				"characteristics": {
					"characteristic": {
						"code": "570b2c9f-0",
						"kind": "1",
						"name": "Пропускная способность",
						"type": "2",
						"actual": "true",
						"values": {
							"value": {
								"OKEI": {
									"code": "2545",
									"name": "Мегабит в секунду"
								},
								"rangeSet": {
									"valueRange": {
										"min": "9000",
										"minMathNotation": "greaterOrEqual"
									}
								},
								"valueFormat": "A"
							}
						},
						"choiceType": "1",
						"isRequired": "true"
					}
				},
				"parentPositionInfo": {
					"code": "61.10.30.190-00000011",
					"version": "1"
				},
				"applicationDateStart": "2018-11-20T00:00:00+03:00"
			}
		}
	}, {
		"id": 112947,
		"code": "61.10.30.190-00000017",
		"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
		"data": {
			"data": {
				"NSI": {
					"classifiers": {
						"classifier": [{
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.10.30.190",
										"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
									}
								}
							}, {
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.20.30.120",
										"name": "Услуги по передаче данных по беспроводным телекоммуникационным сетям"
									}
								}
							}
						]
					}
				},
				"code": "61.10.30.190-00000017",
				"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
				"OKEIs": {
					"OKEI": {
						"code": "876",
						"name": "Условная единица"
					}
				},
				"OKPD2": {
					"code": "61.10.30.190",
					"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
				},
				"actual": "true",
				"version": "1",
				"isTemplate": "false",
				"publishDate": "2018-10-11T17:36:41+04:00",
				"inclusionDate": "2018-10-11T17:36:41+04:00",
				"characteristics": {
					"characteristic": {
						"code": "570b2c9f-0",
						"kind": "1",
						"name": "Пропускная способность",
						"type": "2",
						"actual": "true",
						"values": {
							"value": {
								"OKEI": {
									"code": "2545",
									"name": "Мегабит в секунду"
								},
								"rangeSet": {
									"valueRange": {
										"min": "8000",
										"minMathNotation": "greaterOrEqual"
									}
								},
								"valueFormat": "A"
							}
						},
						"choiceType": "1",
						"isRequired": "true"
					}
				},
				"parentPositionInfo": {
					"code": "61.10.30.190-00000011",
					"version": "1"
				},
				"applicationDateStart": "2018-11-20T00:00:00+03:00"
			}
		}
	}, {
		"id": 112946,
		"code": "61.10.30.190-00000018",
		"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
		"data": {
			"data": {
				"NSI": {
					"classifiers": {
						"classifier": [{
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.10.30.190",
										"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
									}
								}
							}, {
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.20.30.120",
										"name": "Услуги по передаче данных по беспроводным телекоммуникационным сетям"
									}
								}
							}
						]
					}
				},
				"code": "61.10.30.190-00000018",
				"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
				"OKEIs": {
					"OKEI": {
						"code": "876",
						"name": "Условная единица"
					}
				},
				"OKPD2": {
					"code": "61.10.30.190",
					"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
				},
				"actual": "true",
				"version": "1",
				"isTemplate": "false",
				"publishDate": "2018-10-11T17:36:41+04:00",
				"inclusionDate": "2018-10-11T17:36:41+04:00",
				"characteristics": {
					"characteristic": {
						"code": "570b2c9f-0",
						"kind": "1",
						"name": "Пропускная способность",
						"type": "2",
						"actual": "true",
						"values": {
							"value": {
								"OKEI": {
									"code": "2545",
									"name": "Мегабит в секунду"
								},
								"rangeSet": {
									"valueRange": {
										"min": "7000",
										"minMathNotation": "greaterOrEqual"
									}
								},
								"valueFormat": "A"
							}
						},
						"choiceType": "1",
						"isRequired": "true"
					}
				},
				"parentPositionInfo": {
					"code": "61.10.30.190-00000011",
					"version": "1"
				},
				"applicationDateStart": "2018-11-20T00:00:00+03:00"
			}
		}
	}, {
		"id": 112945,
		"code": "61.10.30.190-00000019",
		"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
		"data": {
			"data": {
				"NSI": {
					"classifiers": {
						"classifier": [{
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.10.30.190",
										"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
									}
								}
							}, {
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.20.30.120",
										"name": "Услуги по передаче данных по беспроводным телекоммуникационным сетям"
									}
								}
							}
						]
					}
				},
				"code": "61.10.30.190-00000019",
				"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
				"OKEIs": {
					"OKEI": {
						"code": "876",
						"name": "Условная единица"
					}
				},
				"OKPD2": {
					"code": "61.10.30.190",
					"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
				},
				"actual": "true",
				"version": "1",
				"isTemplate": "false",
				"publishDate": "2018-10-11T17:36:42+04:00",
				"inclusionDate": "2018-10-11T17:36:42+04:00",
				"characteristics": {
					"characteristic": {
						"code": "570b2c9f-0",
						"kind": "1",
						"name": "Пропускная способность",
						"type": "2",
						"actual": "true",
						"values": {
							"value": {
								"OKEI": {
									"code": "2545",
									"name": "Мегабит в секунду"
								},
								"rangeSet": {
									"valueRange": {
										"min": "6000",
										"minMathNotation": "greaterOrEqual"
									}
								},
								"valueFormat": "A"
							}
						},
						"choiceType": "1",
						"isRequired": "true"
					}
				},
				"parentPositionInfo": {
					"code": "61.10.30.190-00000011",
					"version": "1"
				},
				"applicationDateStart": "2018-11-20T00:00:00+03:00"
			}
		}
	}, {
		"id": 112944,
		"code": "61.10.30.190-00000020",
		"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
		"data": {
			"data": {
				"NSI": {
					"classifiers": {
						"classifier": [{
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.20.30.120",
										"name": "Услуги по передаче данных по беспроводным телекоммуникационным сетям"
									}
								}
							}, {
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.10.30.190",
										"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
									}
								}
							}
						]
					}
				},
				"code": "61.10.30.190-00000020",
				"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
				"OKEIs": {
					"OKEI": {
						"code": "876",
						"name": "Условная единица"
					}
				},
				"OKPD2": {
					"code": "61.10.30.190",
					"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
				},
				"actual": "true",
				"version": "1",
				"isTemplate": "false",
				"publishDate": "2018-10-11T17:36:43+04:00",
				"inclusionDate": "2018-10-11T17:36:43+04:00",
				"characteristics": {
					"characteristic": {
						"code": "570b2c9f-0",
						"kind": "1",
						"name": "Пропускная способность",
						"type": "2",
						"actual": "true",
						"values": {
							"value": {
								"OKEI": {
									"code": "2545",
									"name": "Мегабит в секунду"
								},
								"rangeSet": {
									"valueRange": {
										"min": "5000",
										"minMathNotation": "greaterOrEqual"
									}
								},
								"valueFormat": "A"
							}
						},
						"choiceType": "1",
						"isRequired": "true"
					}
				},
				"parentPositionInfo": {
					"code": "61.10.30.190-00000011",
					"version": "1"
				},
				"applicationDateStart": "2018-11-20T00:00:00+03:00"
			}
		}
	}, {
		"id": 112943,
		"code": "61.10.30.190-00000021",
		"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
		"data": {
			"data": {
				"NSI": {
					"classifiers": {
						"classifier": [{
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.10.30.190",
										"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
									}
								}
							}, {
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.20.30.120",
										"name": "Услуги по передаче данных по беспроводным телекоммуникационным сетям"
									}
								}
							}
						]
					}
				},
				"code": "61.10.30.190-00000021",
				"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
				"OKEIs": {
					"OKEI": {
						"code": "876",
						"name": "Условная единица"
					}
				},
				"OKPD2": {
					"code": "61.10.30.190",
					"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
				},
				"actual": "true",
				"version": "1",
				"isTemplate": "false",
				"publishDate": "2018-10-11T17:36:43+04:00",
				"inclusionDate": "2018-10-11T17:36:43+04:00",
				"characteristics": {
					"characteristic": {
						"code": "570b2c9f-0",
						"kind": "1",
						"name": "Пропускная способность",
						"type": "2",
						"actual": "true",
						"values": {
							"value": {
								"OKEI": {
									"code": "2545",
									"name": "Мегабит в секунду"
								},
								"rangeSet": {
									"valueRange": {
										"min": "4000",
										"minMathNotation": "greaterOrEqual"
									}
								},
								"valueFormat": "A"
							}
						},
						"choiceType": "1",
						"isRequired": "true"
					}
				},
				"parentPositionInfo": {
					"code": "61.10.30.190-00000011",
					"version": "1"
				},
				"applicationDateStart": "2018-11-20T00:00:00+03:00"
			}
		}
	}, {
		"id": 112942,
		"code": "61.10.30.190-00000022",
		"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
		"data": {
			"data": {
				"NSI": {
					"classifiers": {
						"classifier": [{
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.10.30.190",
										"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
									}
								}
							}, {
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.20.30.120",
										"name": "Услуги по передаче данных по беспроводным телекоммуникационным сетям"
									}
								}
							}
						]
					}
				},
				"code": "61.10.30.190-00000022",
				"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
				"OKEIs": {
					"OKEI": {
						"code": "876",
						"name": "Условная единица"
					}
				},
				"OKPD2": {
					"code": "61.10.30.190",
					"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
				},
				"actual": "true",
				"version": "1",
				"isTemplate": "false",
				"publishDate": "2018-10-11T17:36:44+04:00",
				"inclusionDate": "2018-10-11T17:36:44+04:00",
				"characteristics": {
					"characteristic": {
						"code": "570b2c9f-0",
						"kind": "1",
						"name": "Пропускная способность",
						"type": "2",
						"actual": "true",
						"values": {
							"value": {
								"OKEI": {
									"code": "2545",
									"name": "Мегабит в секунду"
								},
								"rangeSet": {
									"valueRange": {
										"min": "3000",
										"minMathNotation": "greaterOrEqual"
									}
								},
								"valueFormat": "A"
							}
						},
						"choiceType": "1",
						"isRequired": "true"
					}
				},
				"parentPositionInfo": {
					"code": "61.10.30.190-00000011",
					"version": "1"
				},
				"applicationDateStart": "2018-11-20T00:00:00+03:00"
			}
		}
	}, {
		"id": 112941,
		"code": "61.10.30.190-00000023",
		"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
		"data": {
			"data": {
				"NSI": {
					"classifiers": {
						"classifier": [{
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.10.30.190",
										"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
									}
								}
							}, {
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.20.30.120",
										"name": "Услуги по передаче данных по беспроводным телекоммуникационным сетям"
									}
								}
							}
						]
					}
				},
				"code": "61.10.30.190-00000023",
				"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
				"OKEIs": {
					"OKEI": {
						"code": "876",
						"name": "Условная единица"
					}
				},
				"OKPD2": {
					"code": "61.10.30.190",
					"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
				},
				"actual": "true",
				"version": "1",
				"isTemplate": "false",
				"publishDate": "2018-10-11T17:36:44+04:00",
				"inclusionDate": "2018-10-11T17:36:44+04:00",
				"characteristics": {
					"characteristic": {
						"code": "570b2c9f-0",
						"kind": "1",
						"name": "Пропускная способность",
						"type": "2",
						"actual": "true",
						"values": {
							"value": {
								"OKEI": {
									"code": "2545",
									"name": "Мегабит в секунду"
								},
								"rangeSet": {
									"valueRange": {
										"min": "2000",
										"minMathNotation": "greaterOrEqual"
									}
								},
								"valueFormat": "A"
							}
						},
						"choiceType": "1",
						"isRequired": "true"
					}
				},
				"parentPositionInfo": {
					"code": "61.10.30.190-00000011",
					"version": "1"
				},
				"applicationDateStart": "2018-11-20T00:00:00+03:00"
			}
		}
	}, {
		"id": 112938,
		"code": "61.10.30.190-00000024",
		"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
		"data": {
			"data": {
				"NSI": {
					"classifiers": {
						"classifier": [{
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.10.30.190",
										"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
									}
								}
							}, {
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.20.30.120",
										"name": "Услуги по передаче данных по беспроводным телекоммуникационным сетям"
									}
								}
							}
						]
					}
				},
				"code": "61.10.30.190-00000024",
				"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
				"OKEIs": {
					"OKEI": {
						"code": "876",
						"name": "Условная единица"
					}
				},
				"OKPD2": {
					"code": "61.10.30.190",
					"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
				},
				"actual": "true",
				"version": "1",
				"isTemplate": "false",
				"publishDate": "2018-10-11T17:36:45+04:00",
				"inclusionDate": "2018-10-11T17:36:45+04:00",
				"characteristics": {
					"characteristic": {
						"code": "570b2c9f-0",
						"kind": "1",
						"name": "Пропускная способность",
						"type": "2",
						"actual": "true",
						"values": {
							"value": {
								"OKEI": {
									"code": "2545",
									"name": "Мегабит в секунду"
								},
								"rangeSet": {
									"valueRange": {
										"min": "950",
										"minMathNotation": "greaterOrEqual"
									}
								},
								"valueFormat": "A"
							}
						},
						"choiceType": "1",
						"isRequired": "true"
					}
				},
				"parentPositionInfo": {
					"code": "61.10.30.190-00000011",
					"version": "1"
				},
				"applicationDateStart": "2018-11-20T00:00:00+03:00"
			}
		}
	}, {
		"id": 112937,
		"code": "61.10.30.190-00000025",
		"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
		"data": {
			"data": {
				"NSI": {
					"classifiers": {
						"classifier": [{
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.10.30.190",
										"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
									}
								}
							}, {
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.20.30.120",
										"name": "Услуги по передаче данных по беспроводным телекоммуникационным сетям"
									}
								}
							}
						]
					}
				},
				"code": "61.10.30.190-00000025",
				"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
				"OKEIs": {
					"OKEI": {
						"code": "876",
						"name": "Условная единица"
					}
				},
				"OKPD2": {
					"code": "61.10.30.190",
					"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
				},
				"actual": "true",
				"version": "1",
				"isTemplate": "false",
				"publishDate": "2018-10-11T17:36:45+04:00",
				"inclusionDate": "2018-10-11T17:36:45+04:00",
				"characteristics": {
					"characteristic": {
						"code": "570b2c9f-0",
						"kind": "1",
						"name": "Пропускная способность",
						"type": "2",
						"actual": "true",
						"values": {
							"value": {
								"OKEI": {
									"code": "2545",
									"name": "Мегабит в секунду"
								},
								"rangeSet": {
									"valueRange": {
										"min": "1000",
										"minMathNotation": "greaterOrEqual"
									}
								},
								"valueFormat": "A"
							}
						},
						"choiceType": "1",
						"isRequired": "true"
					}
				},
				"parentPositionInfo": {
					"code": "61.10.30.190-00000011",
					"version": "1"
				},
				"applicationDateStart": "2018-11-20T00:00:00+03:00"
			}
		}
	}, {
		"id": 112936,
		"code": "61.10.30.190-00000026",
		"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
		"data": {
			"data": {
				"NSI": {
					"classifiers": {
						"classifier": [{
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.10.30.190",
										"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
									}
								}
							}, {
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.20.30.120",
										"name": "Услуги по передаче данных по беспроводным телекоммуникационным сетям"
									}
								}
							}
						]
					}
				},
				"code": "61.10.30.190-00000026",
				"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
				"OKEIs": {
					"OKEI": {
						"code": "876",
						"name": "Условная единица"
					}
				},
				"OKPD2": {
					"code": "61.10.30.190",
					"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
				},
				"actual": "true",
				"version": "1",
				"isTemplate": "false",
				"publishDate": "2018-10-11T17:36:46+04:00",
				"inclusionDate": "2018-10-11T17:36:46+04:00",
				"characteristics": {
					"characteristic": {
						"code": "570b2c9f-0",
						"kind": "1",
						"name": "Пропускная способность",
						"type": "2",
						"actual": "true",
						"values": {
							"value": {
								"OKEI": {
									"code": "2545",
									"name": "Мегабит в секунду"
								},
								"rangeSet": {
									"valueRange": {
										"min": "900",
										"minMathNotation": "greaterOrEqual"
									}
								},
								"valueFormat": "A"
							}
						},
						"choiceType": "1",
						"isRequired": "true"
					}
				},
				"parentPositionInfo": {
					"code": "61.10.30.190-00000011",
					"version": "1"
				},
				"applicationDateStart": "2018-11-20T00:00:00+03:00"
			}
		}
	}, {
		"id": 112935,
		"code": "61.10.30.190-00000027",
		"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
		"data": {
			"data": {
				"NSI": {
					"classifiers": {
						"classifier": [{
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.10.30.190",
										"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
									}
								}
							}, {
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.20.30.120",
										"name": "Услуги по передаче данных по беспроводным телекоммуникационным сетям"
									}
								}
							}
						]
					}
				},
				"code": "61.10.30.190-00000027",
				"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
				"OKEIs": {
					"OKEI": {
						"code": "876",
						"name": "Условная единица"
					}
				},
				"OKPD2": {
					"code": "61.10.30.190",
					"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
				},
				"actual": "true",
				"version": "1",
				"isTemplate": "false",
				"publishDate": "2018-10-11T17:36:46+04:00",
				"inclusionDate": "2018-10-11T17:36:46+04:00",
				"characteristics": {
					"characteristic": {
						"code": "570b2c9f-0",
						"kind": "1",
						"name": "Пропускная способность",
						"type": "2",
						"actual": "true",
						"values": {
							"value": {
								"OKEI": {
									"code": "2545",
									"name": "Мегабит в секунду"
								},
								"rangeSet": {
									"valueRange": {
										"min": "850",
										"minMathNotation": "greaterOrEqual"
									}
								},
								"valueFormat": "A"
							}
						},
						"choiceType": "1",
						"isRequired": "true"
					}
				},
				"parentPositionInfo": {
					"code": "61.10.30.190-00000011",
					"version": "1"
				},
				"applicationDateStart": "2018-11-20T00:00:00+03:00"
			}
		}
	}, {
		"id": 112934,
		"code": "61.10.30.190-00000028",
		"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
		"data": {
			"data": {
				"NSI": {
					"classifiers": {
						"classifier": [{
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.10.30.190",
										"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
									}
								}
							}, {
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.20.30.120",
										"name": "Услуги по передаче данных по беспроводным телекоммуникационным сетям"
									}
								}
							}
						]
					}
				},
				"code": "61.10.30.190-00000028",
				"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
				"OKEIs": {
					"OKEI": {
						"code": "876",
						"name": "Условная единица"
					}
				},
				"OKPD2": {
					"code": "61.10.30.190",
					"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
				},
				"actual": "true",
				"version": "1",
				"isTemplate": "false",
				"publishDate": "2018-10-11T17:36:47+04:00",
				"inclusionDate": "2018-10-11T17:36:47+04:00",
				"characteristics": {
					"characteristic": {
						"code": "570b2c9f-0",
						"kind": "1",
						"name": "Пропускная способность",
						"type": "2",
						"actual": "true",
						"values": {
							"value": {
								"OKEI": {
									"code": "2545",
									"name": "Мегабит в секунду"
								},
								"rangeSet": {
									"valueRange": {
										"min": "800",
										"minMathNotation": "greaterOrEqual"
									}
								},
								"valueFormat": "A"
							}
						},
						"choiceType": "1",
						"isRequired": "true"
					}
				},
				"parentPositionInfo": {
					"code": "61.10.30.190-00000011",
					"version": "1"
				},
				"applicationDateStart": "2018-11-20T00:00:00+03:00"
			}
		}
	}, {
		"id": 112933,
		"code": "61.10.30.190-00000029",
		"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
		"data": {
			"data": {
				"NSI": {
					"classifiers": {
						"classifier": [{
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.10.30.190",
										"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
									}
								}
							}, {
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.20.30.120",
										"name": "Услуги по передаче данных по беспроводным телекоммуникационным сетям"
									}
								}
							}
						]
					}
				},
				"code": "61.10.30.190-00000029",
				"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
				"OKEIs": {
					"OKEI": {
						"code": "876",
						"name": "Условная единица"
					}
				},
				"OKPD2": {
					"code": "61.10.30.190",
					"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
				},
				"actual": "true",
				"version": "1",
				"isTemplate": "false",
				"publishDate": "2018-10-11T17:36:47+04:00",
				"inclusionDate": "2018-10-11T17:36:47+04:00",
				"characteristics": {
					"characteristic": {
						"code": "570b2c9f-0",
						"kind": "1",
						"name": "Пропускная способность",
						"type": "2",
						"actual": "true",
						"values": {
							"value": {
								"OKEI": {
									"code": "2545",
									"name": "Мегабит в секунду"
								},
								"rangeSet": {
									"valueRange": {
										"min": "750",
										"minMathNotation": "greaterOrEqual"
									}
								},
								"valueFormat": "A"
							}
						},
						"choiceType": "1",
						"isRequired": "true"
					}
				},
				"parentPositionInfo": {
					"code": "61.10.30.190-00000011",
					"version": "1"
				},
				"applicationDateStart": "2018-11-20T00:00:00+03:00"
			}
		}
	}, {
		"id": 112932,
		"code": "61.10.30.190-00000030",
		"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
		"data": {
			"data": {
				"NSI": {
					"classifiers": {
						"classifier": [{
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.10.30.190",
										"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
									}
								}
							}, {
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.20.30.120",
										"name": "Услуги по передаче данных по беспроводным телекоммуникационным сетям"
									}
								}
							}
						]
					}
				},
				"code": "61.10.30.190-00000030",
				"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
				"OKEIs": {
					"OKEI": {
						"code": "876",
						"name": "Условная единица"
					}
				},
				"OKPD2": {
					"code": "61.10.30.190",
					"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
				},
				"actual": "true",
				"version": "1",
				"isTemplate": "false",
				"publishDate": "2018-10-11T17:36:48+04:00",
				"inclusionDate": "2018-10-11T17:36:48+04:00",
				"characteristics": {
					"characteristic": {
						"code": "570b2c9f-0",
						"kind": "1",
						"name": "Пропускная способность",
						"type": "2",
						"actual": "true",
						"values": {
							"value": {
								"OKEI": {
									"code": "2545",
									"name": "Мегабит в секунду"
								},
								"rangeSet": {
									"valueRange": {
										"min": "700",
										"minMathNotation": "greaterOrEqual"
									}
								},
								"valueFormat": "A"
							}
						},
						"choiceType": "1",
						"isRequired": "true"
					}
				},
				"parentPositionInfo": {
					"code": "61.10.30.190-00000011",
					"version": "1"
				},
				"applicationDateStart": "2018-11-20T00:00:00+03:00"
			}
		}
	}, {
		"id": 112931,
		"code": "61.10.30.190-00000031",
		"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
		"data": {
			"data": {
				"NSI": {
					"classifiers": {
						"classifier": [{
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.10.30.190",
										"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
									}
								}
							}, {
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.20.30.120",
										"name": "Услуги по передаче данных по беспроводным телекоммуникационным сетям"
									}
								}
							}
						]
					}
				},
				"code": "61.10.30.190-00000031",
				"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
				"OKEIs": {
					"OKEI": {
						"code": "876",
						"name": "Условная единица"
					}
				},
				"OKPD2": {
					"code": "61.10.30.190",
					"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
				},
				"actual": "true",
				"version": "1",
				"isTemplate": "false",
				"publishDate": "2018-10-11T17:36:48+04:00",
				"inclusionDate": "2018-10-11T17:36:48+04:00",
				"characteristics": {
					"characteristic": {
						"code": "570b2c9f-0",
						"kind": "1",
						"name": "Пропускная способность",
						"type": "2",
						"actual": "true",
						"values": {
							"value": {
								"OKEI": {
									"code": "2545",
									"name": "Мегабит в секунду"
								},
								"rangeSet": {
									"valueRange": {
										"min": "650",
										"minMathNotation": "greaterOrEqual"
									}
								},
								"valueFormat": "A"
							}
						},
						"choiceType": "1",
						"isRequired": "true"
					}
				},
				"parentPositionInfo": {
					"code": "61.10.30.190-00000011",
					"version": "1"
				},
				"applicationDateStart": "2018-11-20T00:00:00+03:00"
			}
		}
	}, {
		"id": 112930,
		"code": "61.10.30.190-00000032",
		"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
		"data": {
			"data": {
				"NSI": {
					"classifiers": {
						"classifier": [{
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.10.30.190",
										"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
									}
								}
							}, {
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.20.30.120",
										"name": "Услуги по передаче данных по беспроводным телекоммуникационным сетям"
									}
								}
							}
						]
					}
				},
				"code": "61.10.30.190-00000032",
				"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
				"OKEIs": {
					"OKEI": {
						"code": "876",
						"name": "Условная единица"
					}
				},
				"OKPD2": {
					"code": "61.10.30.190",
					"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
				},
				"actual": "true",
				"version": "1",
				"isTemplate": "false",
				"publishDate": "2018-10-11T17:36:49+04:00",
				"inclusionDate": "2018-10-11T17:36:49+04:00",
				"characteristics": {
					"characteristic": {
						"code": "570b2c9f-0",
						"kind": "1",
						"name": "Пропускная способность",
						"type": "2",
						"actual": "true",
						"values": {
							"value": {
								"OKEI": {
									"code": "2545",
									"name": "Мегабит в секунду"
								},
								"rangeSet": {
									"valueRange": {
										"min": "600",
										"minMathNotation": "greaterOrEqual"
									}
								},
								"valueFormat": "A"
							}
						},
						"choiceType": "1",
						"isRequired": "true"
					}
				},
				"parentPositionInfo": {
					"code": "61.10.30.190-00000011",
					"version": "1"
				},
				"applicationDateStart": "2018-11-20T00:00:00+03:00"
			}
		}
	}, {
		"id": 112929,
		"code": "61.10.30.190-00000033",
		"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
		"data": {
			"data": {
				"NSI": {
					"classifiers": {
						"classifier": [{
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.10.30.190",
										"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
									}
								}
							}, {
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.20.30.120",
										"name": "Услуги по передаче данных по беспроводным телекоммуникационным сетям"
									}
								}
							}
						]
					}
				},
				"code": "61.10.30.190-00000033",
				"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
				"OKEIs": {
					"OKEI": {
						"code": "876",
						"name": "Условная единица"
					}
				},
				"OKPD2": {
					"code": "61.10.30.190",
					"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
				},
				"actual": "true",
				"version": "1",
				"isTemplate": "false",
				"publishDate": "2018-10-11T17:36:49+04:00",
				"inclusionDate": "2018-10-11T17:36:49+04:00",
				"characteristics": {
					"characteristic": {
						"code": "570b2c9f-0",
						"kind": "1",
						"name": "Пропускная способность",
						"type": "2",
						"actual": "true",
						"values": {
							"value": {
								"OKEI": {
									"code": "2545",
									"name": "Мегабит в секунду"
								},
								"rangeSet": {
									"valueRange": {
										"min": "550",
										"minMathNotation": "greaterOrEqual"
									}
								},
								"valueFormat": "A"
							}
						},
						"choiceType": "1",
						"isRequired": "true"
					}
				},
				"parentPositionInfo": {
					"code": "61.10.30.190-00000011",
					"version": "1"
				},
				"applicationDateStart": "2018-11-20T00:00:00+03:00"
			}
		}
	}, {
		"id": 112928,
		"code": "61.10.30.190-00000034",
		"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
		"data": {
			"data": {
				"NSI": {
					"classifiers": {
						"classifier": [{
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.10.30.190",
										"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
									}
								}
							}, {
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.20.30.120",
										"name": "Услуги по передаче данных по беспроводным телекоммуникационным сетям"
									}
								}
							}
						]
					}
				},
				"code": "61.10.30.190-00000034",
				"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
				"OKEIs": {
					"OKEI": {
						"code": "876",
						"name": "Условная единица"
					}
				},
				"OKPD2": {
					"code": "61.10.30.190",
					"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
				},
				"actual": "true",
				"version": "1",
				"isTemplate": "false",
				"publishDate": "2018-10-11T17:36:49+04:00",
				"inclusionDate": "2018-10-11T17:36:49+04:00",
				"characteristics": {
					"characteristic": {
						"code": "570b2c9f-0",
						"kind": "1",
						"name": "Пропускная способность",
						"type": "2",
						"actual": "true",
						"values": {
							"value": {
								"OKEI": {
									"code": "2545",
									"name": "Мегабит в секунду"
								},
								"rangeSet": {
									"valueRange": {
										"min": "500",
										"minMathNotation": "greaterOrEqual"
									}
								},
								"valueFormat": "A"
							}
						},
						"choiceType": "1",
						"isRequired": "true"
					}
				},
				"parentPositionInfo": {
					"code": "61.10.30.190-00000011",
					"version": "1"
				},
				"applicationDateStart": "2018-11-20T00:00:00+03:00"
			}
		}
	}, {
		"id": 112927,
		"code": "61.10.30.190-00000035",
		"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
		"data": {
			"data": {
				"NSI": {
					"classifiers": {
						"classifier": [{
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.10.30.190",
										"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
									}
								}
							}, {
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.20.30.120",
										"name": "Услуги по передаче данных по беспроводным телекоммуникационным сетям"
									}
								}
							}
						]
					}
				},
				"code": "61.10.30.190-00000035",
				"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
				"OKEIs": {
					"OKEI": {
						"code": "876",
						"name": "Условная единица"
					}
				},
				"OKPD2": {
					"code": "61.10.30.190",
					"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
				},
				"actual": "true",
				"version": "1",
				"isTemplate": "false",
				"publishDate": "2018-10-11T17:36:50+04:00",
				"inclusionDate": "2018-10-11T17:36:50+04:00",
				"characteristics": {
					"characteristic": {
						"code": "570b2c9f-0",
						"kind": "1",
						"name": "Пропускная способность",
						"type": "2",
						"actual": "true",
						"values": {
							"value": {
								"OKEI": {
									"code": "2545",
									"name": "Мегабит в секунду"
								},
								"rangeSet": {
									"valueRange": {
										"min": "450",
										"minMathNotation": "greaterOrEqual"
									}
								},
								"valueFormat": "A"
							}
						},
						"choiceType": "1",
						"isRequired": "true"
					}
				},
				"parentPositionInfo": {
					"code": "61.10.30.190-00000011",
					"version": "1"
				},
				"applicationDateStart": "2018-11-20T00:00:00+03:00"
			}
		}
	}, {
		"id": 112926,
		"code": "61.10.30.190-00000036",
		"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
		"data": {
			"data": {
				"NSI": {
					"classifiers": {
						"classifier": [{
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.10.30.190",
										"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
									}
								}
							}, {
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.20.30.120",
										"name": "Услуги по передаче данных по беспроводным телекоммуникационным сетям"
									}
								}
							}
						]
					}
				},
				"code": "61.10.30.190-00000036",
				"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
				"OKEIs": {
					"OKEI": {
						"code": "876",
						"name": "Условная единица"
					}
				},
				"OKPD2": {
					"code": "61.10.30.190",
					"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
				},
				"actual": "true",
				"version": "1",
				"isTemplate": "false",
				"publishDate": "2018-10-11T17:36:51+04:00",
				"inclusionDate": "2018-10-11T17:36:51+04:00",
				"characteristics": {
					"characteristic": {
						"code": "570b2c9f-0",
						"kind": "1",
						"name": "Пропускная способность",
						"type": "2",
						"actual": "true",
						"values": {
							"value": {
								"OKEI": {
									"code": "2545",
									"name": "Мегабит в секунду"
								},
								"rangeSet": {
									"valueRange": {
										"min": "400",
										"minMathNotation": "greaterOrEqual"
									}
								},
								"valueFormat": "A"
							}
						},
						"choiceType": "1",
						"isRequired": "true"
					}
				},
				"parentPositionInfo": {
					"code": "61.10.30.190-00000011",
					"version": "1"
				},
				"applicationDateStart": "2018-11-20T00:00:00+03:00"
			}
		}
	}, {
		"id": 112925,
		"code": "61.10.30.190-00000037",
		"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
		"data": {
			"data": {
				"NSI": {
					"classifiers": {
						"classifier": [{
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.10.30.190",
										"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
									}
								}
							}, {
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.20.30.120",
										"name": "Услуги по передаче данных по беспроводным телекоммуникационным сетям"
									}
								}
							}
						]
					}
				},
				"code": "61.10.30.190-00000037",
				"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
				"OKEIs": {
					"OKEI": {
						"code": "876",
						"name": "Условная единица"
					}
				},
				"OKPD2": {
					"code": "61.10.30.190",
					"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
				},
				"actual": "true",
				"version": "1",
				"isTemplate": "false",
				"publishDate": "2018-10-11T17:36:51+04:00",
				"inclusionDate": "2018-10-11T17:36:51+04:00",
				"characteristics": {
					"characteristic": {
						"code": "570b2c9f-0",
						"kind": "1",
						"name": "Пропускная способность",
						"type": "2",
						"actual": "true",
						"values": {
							"value": {
								"OKEI": {
									"code": "2545",
									"name": "Мегабит в секунду"
								},
								"rangeSet": {
									"valueRange": {
										"min": "350",
										"minMathNotation": "greaterOrEqual"
									}
								},
								"valueFormat": "A"
							}
						},
						"choiceType": "1",
						"isRequired": "true"
					}
				},
				"parentPositionInfo": {
					"code": "61.10.30.190-00000011",
					"version": "1"
				},
				"applicationDateStart": "2018-11-20T00:00:00+03:00"
			}
		}
	}, {
		"id": 112924,
		"code": "61.10.30.190-00000038",
		"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
		"data": {
			"data": {
				"NSI": {
					"classifiers": {
						"classifier": [{
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.10.30.190",
										"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
									}
								}
							}, {
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.20.30.120",
										"name": "Услуги по передаче данных по беспроводным телекоммуникационным сетям"
									}
								}
							}
						]
					}
				},
				"code": "61.10.30.190-00000038",
				"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
				"OKEIs": {
					"OKEI": {
						"code": "876",
						"name": "Условная единица"
					}
				},
				"OKPD2": {
					"code": "61.10.30.190",
					"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
				},
				"actual": "true",
				"version": "1",
				"isTemplate": "false",
				"publishDate": "2018-10-11T17:36:52+04:00",
				"inclusionDate": "2018-10-11T17:36:52+04:00",
				"characteristics": {
					"characteristic": {
						"code": "570b2c9f-0",
						"kind": "1",
						"name": "Пропускная способность",
						"type": "2",
						"actual": "true",
						"values": {
							"value": {
								"OKEI": {
									"code": "2545",
									"name": "Мегабит в секунду"
								},
								"rangeSet": {
									"valueRange": {
										"min": "300",
										"minMathNotation": "greaterOrEqual"
									}
								},
								"valueFormat": "A"
							}
						},
						"choiceType": "1",
						"isRequired": "true"
					}
				},
				"parentPositionInfo": {
					"code": "61.10.30.190-00000011",
					"version": "1"
				},
				"applicationDateStart": "2018-11-20T00:00:00+03:00"
			}
		}
	}, {
		"id": 112923,
		"code": "61.10.30.190-00000039",
		"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
		"data": {
			"data": {
				"NSI": {
					"classifiers": {
						"classifier": [{
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.10.30.190",
										"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
									}
								}
							}, {
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.20.30.120",
										"name": "Услуги по передаче данных по беспроводным телекоммуникационным сетям"
									}
								}
							}
						]
					}
				},
				"code": "61.10.30.190-00000039",
				"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
				"OKEIs": {
					"OKEI": {
						"code": "876",
						"name": "Условная единица"
					}
				},
				"OKPD2": {
					"code": "61.10.30.190",
					"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
				},
				"actual": "true",
				"version": "1",
				"isTemplate": "false",
				"publishDate": "2018-10-11T17:36:52+04:00",
				"inclusionDate": "2018-10-11T17:36:52+04:00",
				"characteristics": {
					"characteristic": {
						"code": "570b2c9f-0",
						"kind": "1",
						"name": "Пропускная способность",
						"type": "2",
						"actual": "true",
						"values": {
							"value": {
								"OKEI": {
									"code": "2545",
									"name": "Мегабит в секунду"
								},
								"rangeSet": {
									"valueRange": {
										"min": "250",
										"minMathNotation": "greaterOrEqual"
									}
								},
								"valueFormat": "A"
							}
						},
						"choiceType": "1",
						"isRequired": "true"
					}
				},
				"parentPositionInfo": {
					"code": "61.10.30.190-00000011",
					"version": "1"
				},
				"applicationDateStart": "2018-11-20T00:00:00+03:00"
			}
		}
	}, {
		"id": 112922,
		"code": "61.10.30.190-00000040",
		"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
		"data": {
			"data": {
				"NSI": {
					"classifiers": {
						"classifier": [{
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.10.30.190",
										"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
									}
								}
							}, {
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.20.30.120",
										"name": "Услуги по передаче данных по беспроводным телекоммуникационным сетям"
									}
								}
							}
						]
					}
				},
				"code": "61.10.30.190-00000040",
				"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
				"OKEIs": {
					"OKEI": {
						"code": "876",
						"name": "Условная единица"
					}
				},
				"OKPD2": {
					"code": "61.10.30.190",
					"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
				},
				"actual": "true",
				"version": "1",
				"isTemplate": "false",
				"publishDate": "2018-10-11T17:36:53+04:00",
				"inclusionDate": "2018-10-11T17:36:53+04:00",
				"characteristics": {
					"characteristic": {
						"code": "570b2c9f-0",
						"kind": "1",
						"name": "Пропускная способность",
						"type": "2",
						"actual": "true",
						"values": {
							"value": {
								"OKEI": {
									"code": "2545",
									"name": "Мегабит в секунду"
								},
								"rangeSet": {
									"valueRange": {
										"min": "190",
										"minMathNotation": "greaterOrEqual"
									}
								},
								"valueFormat": "A"
							}
						},
						"choiceType": "1",
						"isRequired": "true"
					}
				},
				"parentPositionInfo": {
					"code": "61.10.30.190-00000011",
					"version": "1"
				},
				"applicationDateStart": "2018-11-20T00:00:00+03:00"
			}
		}
	}, {
		"id": 112921,
		"code": "61.10.30.190-00000041",
		"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
		"data": {
			"data": {
				"NSI": {
					"classifiers": {
						"classifier": [{
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.10.30.190",
										"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
									}
								}
							}, {
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.20.30.120",
										"name": "Услуги по передаче данных по беспроводным телекоммуникационным сетям"
									}
								}
							}
						]
					}
				},
				"code": "61.10.30.190-00000041",
				"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
				"OKEIs": {
					"OKEI": {
						"code": "876",
						"name": "Условная единица"
					}
				},
				"OKPD2": {
					"code": "61.10.30.190",
					"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
				},
				"actual": "true",
				"version": "1",
				"isTemplate": "false",
				"publishDate": "2018-10-11T17:36:53+04:00",
				"inclusionDate": "2018-10-11T17:36:53+04:00",
				"characteristics": {
					"characteristic": {
						"code": "570b2c9f-0",
						"kind": "1",
						"name": "Пропускная способность",
						"type": "2",
						"actual": "true",
						"values": {
							"value": {
								"OKEI": {
									"code": "2545",
									"name": "Мегабит в секунду"
								},
								"rangeSet": {
									"valueRange": {
										"min": "200",
										"minMathNotation": "greaterOrEqual"
									}
								},
								"valueFormat": "A"
							}
						},
						"choiceType": "1",
						"isRequired": "true"
					}
				},
				"parentPositionInfo": {
					"code": "61.10.30.190-00000011",
					"version": "1"
				},
				"applicationDateStart": "2018-11-20T00:00:00+03:00"
			}
		}
	}, {
		"id": 112920,
		"code": "61.10.30.190-00000042",
		"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
		"data": {
			"data": {
				"NSI": {
					"classifiers": {
						"classifier": [{
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.10.30.190",
										"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
									}
								}
							}, {
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.20.30.120",
										"name": "Услуги по передаче данных по беспроводным телекоммуникационным сетям"
									}
								}
							}
						]
					}
				},
				"code": "61.10.30.190-00000042",
				"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
				"OKEIs": {
					"OKEI": {
						"code": "876",
						"name": "Условная единица"
					}
				},
				"OKPD2": {
					"code": "61.10.30.190",
					"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
				},
				"actual": "true",
				"version": "1",
				"isTemplate": "false",
				"publishDate": "2018-10-11T17:36:54+04:00",
				"inclusionDate": "2018-10-11T17:36:54+04:00",
				"characteristics": {
					"characteristic": {
						"code": "570b2c9f-0",
						"kind": "1",
						"name": "Пропускная способность",
						"type": "2",
						"actual": "true",
						"values": {
							"value": {
								"OKEI": {
									"code": "2545",
									"name": "Мегабит в секунду"
								},
								"rangeSet": {
									"valueRange": {
										"min": "180",
										"minMathNotation": "greaterOrEqual"
									}
								},
								"valueFormat": "A"
							}
						},
						"choiceType": "1",
						"isRequired": "true"
					}
				},
				"parentPositionInfo": {
					"code": "61.10.30.190-00000011",
					"version": "1"
				},
				"applicationDateStart": "2018-11-20T00:00:00+03:00"
			}
		}
	}, {
		"id": 112919,
		"code": "61.10.30.190-00000043",
		"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
		"data": {
			"data": {
				"NSI": {
					"classifiers": {
						"classifier": [{
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.10.30.190",
										"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
									}
								}
							}, {
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.20.30.120",
										"name": "Услуги по передаче данных по беспроводным телекоммуникационным сетям"
									}
								}
							}
						]
					}
				},
				"code": "61.10.30.190-00000043",
				"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
				"OKEIs": {
					"OKEI": {
						"code": "876",
						"name": "Условная единица"
					}
				},
				"OKPD2": {
					"code": "61.10.30.190",
					"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
				},
				"actual": "true",
				"version": "1",
				"isTemplate": "false",
				"publishDate": "2018-10-11T17:36:54+04:00",
				"inclusionDate": "2018-10-11T17:36:54+04:00",
				"characteristics": {
					"characteristic": {
						"code": "570b2c9f-0",
						"kind": "1",
						"name": "Пропускная способность",
						"type": "2",
						"actual": "true",
						"values": {
							"value": {
								"OKEI": {
									"code": "2545",
									"name": "Мегабит в секунду"
								},
								"rangeSet": {
									"valueRange": {
										"min": "170",
										"minMathNotation": "greaterOrEqual"
									}
								},
								"valueFormat": "A"
							}
						},
						"choiceType": "1",
						"isRequired": "true"
					}
				},
				"parentPositionInfo": {
					"code": "61.10.30.190-00000011",
					"version": "1"
				},
				"applicationDateStart": "2018-11-20T00:00:00+03:00"
			}
		}
	}, {
		"id": 112918,
		"code": "61.10.30.190-00000044",
		"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
		"data": {
			"data": {
				"NSI": {
					"classifiers": {
						"classifier": [{
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.10.30.190",
										"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
									}
								}
							}, {
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.20.30.120",
										"name": "Услуги по передаче данных по беспроводным телекоммуникационным сетям"
									}
								}
							}
						]
					}
				},
				"code": "61.10.30.190-00000044",
				"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
				"OKEIs": {
					"OKEI": {
						"code": "876",
						"name": "Условная единица"
					}
				},
				"OKPD2": {
					"code": "61.10.30.190",
					"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
				},
				"actual": "true",
				"version": "1",
				"isTemplate": "false",
				"publishDate": "2018-10-11T17:36:55+04:00",
				"inclusionDate": "2018-10-11T17:36:55+04:00",
				"characteristics": {
					"characteristic": {
						"code": "570b2c9f-0",
						"kind": "1",
						"name": "Пропускная способность",
						"type": "2",
						"actual": "true",
						"values": {
							"value": {
								"OKEI": {
									"code": "2545",
									"name": "Мегабит в секунду"
								},
								"rangeSet": {
									"valueRange": {
										"min": "160",
										"minMathNotation": "greaterOrEqual"
									}
								},
								"valueFormat": "A"
							}
						},
						"choiceType": "1",
						"isRequired": "true"
					}
				},
				"parentPositionInfo": {
					"code": "61.10.30.190-00000011",
					"version": "1"
				},
				"applicationDateStart": "2018-11-20T00:00:00+03:00"
			}
		}
	}, {
		"id": 112917,
		"code": "61.10.30.190-00000045",
		"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
		"data": {
			"data": {
				"NSI": {
					"classifiers": {
						"classifier": [{
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.10.30.190",
										"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
									}
								}
							}, {
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.20.30.120",
										"name": "Услуги по передаче данных по беспроводным телекоммуникационным сетям"
									}
								}
							}
						]
					}
				},
				"code": "61.10.30.190-00000045",
				"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
				"OKEIs": {
					"OKEI": {
						"code": "876",
						"name": "Условная единица"
					}
				},
				"OKPD2": {
					"code": "61.10.30.190",
					"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
				},
				"actual": "true",
				"version": "1",
				"isTemplate": "false",
				"publishDate": "2018-10-11T17:36:55+04:00",
				"inclusionDate": "2018-10-11T17:36:55+04:00",
				"characteristics": {
					"characteristic": {
						"code": "570b2c9f-0",
						"kind": "1",
						"name": "Пропускная способность",
						"type": "2",
						"actual": "true",
						"values": {
							"value": {
								"OKEI": {
									"code": "2545",
									"name": "Мегабит в секунду"
								},
								"rangeSet": {
									"valueRange": {
										"min": "150",
										"minMathNotation": "greaterOrEqual"
									}
								},
								"valueFormat": "A"
							}
						},
						"choiceType": "1",
						"isRequired": "true"
					}
				},
				"parentPositionInfo": {
					"code": "61.10.30.190-00000011",
					"version": "1"
				},
				"applicationDateStart": "2018-11-20T00:00:00+03:00"
			}
		}
	}, {
		"id": 112916,
		"code": "61.10.30.190-00000046",
		"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
		"data": {
			"data": {
				"NSI": {
					"classifiers": {
						"classifier": [{
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.10.30.190",
										"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
									}
								}
							}, {
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.20.30.120",
										"name": "Услуги по передаче данных по беспроводным телекоммуникационным сетям"
									}
								}
							}
						]
					}
				},
				"code": "61.10.30.190-00000046",
				"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
				"OKEIs": {
					"OKEI": {
						"code": "876",
						"name": "Условная единица"
					}
				},
				"OKPD2": {
					"code": "61.10.30.190",
					"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
				},
				"actual": "true",
				"version": "1",
				"isTemplate": "false",
				"publishDate": "2018-10-11T17:36:56+04:00",
				"inclusionDate": "2018-10-11T17:36:56+04:00",
				"characteristics": {
					"characteristic": {
						"code": "570b2c9f-0",
						"kind": "1",
						"name": "Пропускная способность",
						"type": "2",
						"actual": "true",
						"values": {
							"value": {
								"OKEI": {
									"code": "2545",
									"name": "Мегабит в секунду"
								},
								"rangeSet": {
									"valueRange": {
										"min": "140",
										"minMathNotation": "greaterOrEqual"
									}
								},
								"valueFormat": "A"
							}
						},
						"choiceType": "1",
						"isRequired": "true"
					}
				},
				"parentPositionInfo": {
					"code": "61.10.30.190-00000011",
					"version": "1"
				},
				"applicationDateStart": "2018-11-20T00:00:00+03:00"
			}
		}
	}, {
		"id": 112915,
		"code": "61.10.30.190-00000047",
		"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
		"data": {
			"data": {
				"NSI": {
					"classifiers": {
						"classifier": [{
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.10.30.190",
										"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
									}
								}
							}, {
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.20.30.120",
										"name": "Услуги по передаче данных по беспроводным телекоммуникационным сетям"
									}
								}
							}
						]
					}
				},
				"code": "61.10.30.190-00000047",
				"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
				"OKEIs": {
					"OKEI": {
						"code": "876",
						"name": "Условная единица"
					}
				},
				"OKPD2": {
					"code": "61.10.30.190",
					"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
				},
				"actual": "true",
				"version": "1",
				"isTemplate": "false",
				"publishDate": "2018-10-11T17:36:56+04:00",
				"inclusionDate": "2018-10-11T17:36:56+04:00",
				"characteristics": {
					"characteristic": {
						"code": "570b2c9f-0",
						"kind": "1",
						"name": "Пропускная способность",
						"type": "2",
						"actual": "true",
						"values": {
							"value": {
								"OKEI": {
									"code": "2545",
									"name": "Мегабит в секунду"
								},
								"rangeSet": {
									"valueRange": {
										"min": "130",
										"minMathNotation": "greaterOrEqual"
									}
								},
								"valueFormat": "A"
							}
						},
						"choiceType": "1",
						"isRequired": "true"
					}
				},
				"parentPositionInfo": {
					"code": "61.10.30.190-00000011",
					"version": "1"
				},
				"applicationDateStart": "2018-11-20T00:00:00+03:00"
			}
		}
	}, {
		"id": 112914,
		"code": "61.10.30.190-00000048",
		"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
		"data": {
			"data": {
				"NSI": {
					"classifiers": {
						"classifier": [{
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.10.30.190",
										"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
									}
								}
							}, {
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.20.30.120",
										"name": "Услуги по передаче данных по беспроводным телекоммуникационным сетям"
									}
								}
							}
						]
					}
				},
				"code": "61.10.30.190-00000048",
				"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
				"OKEIs": {
					"OKEI": {
						"code": "876",
						"name": "Условная единица"
					}
				},
				"OKPD2": {
					"code": "61.10.30.190",
					"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
				},
				"actual": "true",
				"version": "1",
				"isTemplate": "false",
				"publishDate": "2018-10-11T17:36:57+04:00",
				"inclusionDate": "2018-10-11T17:36:57+04:00",
				"characteristics": {
					"characteristic": {
						"code": "570b2c9f-0",
						"kind": "1",
						"name": "Пропускная способность",
						"type": "2",
						"actual": "true",
						"values": {
							"value": {
								"OKEI": {
									"code": "2545",
									"name": "Мегабит в секунду"
								},
								"rangeSet": {
									"valueRange": {
										"min": "120",
										"minMathNotation": "greaterOrEqual"
									}
								},
								"valueFormat": "A"
							}
						},
						"choiceType": "1",
						"isRequired": "true"
					}
				},
				"parentPositionInfo": {
					"code": "61.10.30.190-00000011",
					"version": "1"
				},
				"applicationDateStart": "2018-11-20T00:00:00+03:00"
			}
		}
	}, {
		"id": 112913,
		"code": "61.10.30.190-00000049",
		"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
		"data": {
			"data": {
				"NSI": {
					"classifiers": {
						"classifier": [{
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.10.30.190",
										"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
									}
								}
							}, {
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.20.30.120",
										"name": "Услуги по передаче данных по беспроводным телекоммуникационным сетям"
									}
								}
							}
						]
					}
				},
				"code": "61.10.30.190-00000049",
				"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
				"OKEIs": {
					"OKEI": {
						"code": "876",
						"name": "Условная единица"
					}
				},
				"OKPD2": {
					"code": "61.10.30.190",
					"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
				},
				"actual": "true",
				"version": "1",
				"isTemplate": "false",
				"publishDate": "2018-10-11T17:36:57+04:00",
				"inclusionDate": "2018-10-11T17:36:57+04:00",
				"characteristics": {
					"characteristic": {
						"code": "570b2c9f-0",
						"kind": "1",
						"name": "Пропускная способность",
						"type": "2",
						"actual": "true",
						"values": {
							"value": {
								"OKEI": {
									"code": "2545",
									"name": "Мегабит в секунду"
								},
								"rangeSet": {
									"valueRange": {
										"min": "110",
										"minMathNotation": "greaterOrEqual"
									}
								},
								"valueFormat": "A"
							}
						},
						"choiceType": "1",
						"isRequired": "true"
					}
				},
				"parentPositionInfo": {
					"code": "61.10.30.190-00000011",
					"version": "1"
				},
				"applicationDateStart": "2018-11-20T00:00:00+03:00"
			}
		}
	}, {
		"id": 112912,
		"code": "61.10.30.190-00000050",
		"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
		"data": {
			"data": {
				"NSI": {
					"classifiers": {
						"classifier": [{
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.10.30.190",
										"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
									}
								}
							}, {
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.20.30.120",
										"name": "Услуги по передаче данных по беспроводным телекоммуникационным сетям"
									}
								}
							}
						]
					}
				},
				"code": "61.10.30.190-00000050",
				"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
				"OKEIs": {
					"OKEI": {
						"code": "876",
						"name": "Условная единица"
					}
				},
				"OKPD2": {
					"code": "61.10.30.190",
					"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
				},
				"actual": "true",
				"version": "1",
				"isTemplate": "false",
				"publishDate": "2018-10-11T17:36:58+04:00",
				"inclusionDate": "2018-10-11T17:36:58+04:00",
				"characteristics": {
					"characteristic": {
						"code": "570b2c9f-0",
						"kind": "1",
						"name": "Пропускная способность",
						"type": "2",
						"actual": "true",
						"values": {
							"value": {
								"OKEI": {
									"code": "2545",
									"name": "Мегабит в секунду"
								},
								"rangeSet": {
									"valueRange": {
										"min": "5",
										"minMathNotation": "greaterOrEqual"
									}
								},
								"valueFormat": "A"
							}
						},
						"choiceType": "1",
						"isRequired": "true"
					}
				},
				"parentPositionInfo": {
					"code": "61.10.30.190-00000011",
					"version": "1"
				},
				"applicationDateStart": "2018-11-20T00:00:00+03:00"
			}
		}
	}, {
		"id": 112911,
		"code": "61.10.30.190-00000051",
		"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
		"data": {
			"data": {
				"NSI": {
					"classifiers": {
						"classifier": [{
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.10.30.190",
										"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
									}
								}
							}, {
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.20.30.120",
										"name": "Услуги по передаче данных по беспроводным телекоммуникационным сетям"
									}
								}
							}
						]
					}
				},
				"code": "61.10.30.190-00000051",
				"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
				"OKEIs": {
					"OKEI": {
						"code": "876",
						"name": "Условная единица"
					}
				},
				"OKPD2": {
					"code": "61.10.30.190",
					"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
				},
				"actual": "true",
				"version": "1",
				"isTemplate": "false",
				"publishDate": "2018-10-11T17:36:59+04:00",
				"inclusionDate": "2018-10-11T17:36:59+04:00",
				"characteristics": {
					"characteristic": {
						"code": "570b2c9f-0",
						"kind": "1",
						"name": "Пропускная способность",
						"type": "2",
						"actual": "true",
						"values": {
							"value": {
								"OKEI": {
									"code": "2545",
									"name": "Мегабит в секунду"
								},
								"rangeSet": {
									"valueRange": {
										"min": "100",
										"minMathNotation": "greaterOrEqual"
									}
								},
								"valueFormat": "A"
							}
						},
						"choiceType": "1",
						"isRequired": "true"
					}
				},
				"parentPositionInfo": {
					"code": "61.10.30.190-00000011",
					"version": "1"
				},
				"applicationDateStart": "2018-11-20T00:00:00+03:00"
			}
		}
	}, {
		"id": 112910,
		"code": "61.10.30.190-00000052",
		"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
		"data": {
			"data": {
				"NSI": {
					"classifiers": {
						"classifier": [{
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.10.30.190",
										"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
									}
								}
							}, {
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.20.30.120",
										"name": "Услуги по передаче данных по беспроводным телекоммуникационным сетям"
									}
								}
							}
						]
					}
				},
				"code": "61.10.30.190-00000052",
				"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
				"OKEIs": {
					"OKEI": {
						"code": "876",
						"name": "Условная единица"
					}
				},
				"OKPD2": {
					"code": "61.10.30.190",
					"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
				},
				"actual": "true",
				"version": "1",
				"isTemplate": "false",
				"publishDate": "2018-10-11T17:36:59+04:00",
				"inclusionDate": "2018-10-11T17:36:59+04:00",
				"characteristics": {
					"characteristic": {
						"code": "570b2c9f-0",
						"kind": "1",
						"name": "Пропускная способность",
						"type": "2",
						"actual": "true",
						"values": {
							"value": {
								"OKEI": {
									"code": "2545",
									"name": "Мегабит в секунду"
								},
								"rangeSet": {
									"valueRange": {
										"min": "90",
										"minMathNotation": "greaterOrEqual"
									}
								},
								"valueFormat": "A"
							}
						},
						"choiceType": "1",
						"isRequired": "true"
					}
				},
				"parentPositionInfo": {
					"code": "61.10.30.190-00000011",
					"version": "1"
				},
				"applicationDateStart": "2018-11-20T00:00:00+03:00"
			}
		}
	}, {
		"id": 112909,
		"code": "61.10.30.190-00000053",
		"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
		"data": {
			"data": {
				"NSI": {
					"classifiers": {
						"classifier": [{
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.10.30.190",
										"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
									}
								}
							}, {
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.20.30.120",
										"name": "Услуги по передаче данных по беспроводным телекоммуникационным сетям"
									}
								}
							}
						]
					}
				},
				"code": "61.10.30.190-00000053",
				"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
				"OKEIs": {
					"OKEI": {
						"code": "876",
						"name": "Условная единица"
					}
				},
				"OKPD2": {
					"code": "61.10.30.190",
					"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
				},
				"actual": "true",
				"version": "1",
				"isTemplate": "false",
				"publishDate": "2018-10-11T17:37:00+04:00",
				"inclusionDate": "2018-10-11T17:37:00+04:00",
				"characteristics": {
					"characteristic": {
						"code": "570b2c9f-0",
						"kind": "1",
						"name": "Пропускная способность",
						"type": "2",
						"actual": "true",
						"values": {
							"value": {
								"OKEI": {
									"code": "2545",
									"name": "Мегабит в секунду"
								},
								"rangeSet": {
									"valueRange": {
										"min": "80",
										"minMathNotation": "greaterOrEqual"
									}
								},
								"valueFormat": "A"
							}
						},
						"choiceType": "1",
						"isRequired": "true"
					}
				},
				"parentPositionInfo": {
					"code": "61.10.30.190-00000011",
					"version": "1"
				},
				"applicationDateStart": "2018-11-20T00:00:00+03:00"
			}
		}
	}, {
		"id": 112908,
		"code": "61.10.30.190-00000054",
		"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
		"data": {
			"data": {
				"NSI": {
					"classifiers": {
						"classifier": [{
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.10.30.190",
										"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
									}
								}
							}, {
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.20.30.120",
										"name": "Услуги по передаче данных по беспроводным телекоммуникационным сетям"
									}
								}
							}
						]
					}
				},
				"code": "61.10.30.190-00000054",
				"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
				"OKEIs": {
					"OKEI": {
						"code": "876",
						"name": "Условная единица"
					}
				},
				"OKPD2": {
					"code": "61.10.30.190",
					"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
				},
				"actual": "true",
				"version": "1",
				"isTemplate": "false",
				"publishDate": "2018-10-11T17:37:00+04:00",
				"inclusionDate": "2018-10-11T17:37:00+04:00",
				"characteristics": {
					"characteristic": {
						"code": "570b2c9f-0",
						"kind": "1",
						"name": "Пропускная способность",
						"type": "2",
						"actual": "true",
						"values": {
							"value": {
								"OKEI": {
									"code": "2545",
									"name": "Мегабит в секунду"
								},
								"rangeSet": {
									"valueRange": {
										"min": "70",
										"minMathNotation": "greaterOrEqual"
									}
								},
								"valueFormat": "A"
							}
						},
						"choiceType": "1",
						"isRequired": "true"
					}
				},
				"parentPositionInfo": {
					"code": "61.10.30.190-00000011",
					"version": "1"
				},
				"applicationDateStart": "2018-11-20T00:00:00+03:00"
			}
		}
	}, {
		"id": 112907,
		"code": "61.10.30.190-00000055",
		"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
		"data": {
			"data": {
				"NSI": {
					"classifiers": {
						"classifier": [{
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.10.30.190",
										"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
									}
								}
							}, {
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.20.30.120",
										"name": "Услуги по передаче данных по беспроводным телекоммуникационным сетям"
									}
								}
							}
						]
					}
				},
				"code": "61.10.30.190-00000055",
				"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
				"OKEIs": {
					"OKEI": {
						"code": "876",
						"name": "Условная единица"
					}
				},
				"OKPD2": {
					"code": "61.10.30.190",
					"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
				},
				"actual": "true",
				"version": "1",
				"isTemplate": "false",
				"publishDate": "2018-10-11T17:37:01+04:00",
				"inclusionDate": "2018-10-11T17:37:01+04:00",
				"characteristics": {
					"characteristic": {
						"code": "570b2c9f-0",
						"kind": "1",
						"name": "Пропускная способность",
						"type": "2",
						"actual": "true",
						"values": {
							"value": {
								"OKEI": {
									"code": "2545",
									"name": "Мегабит в секунду"
								},
								"rangeSet": {
									"valueRange": {
										"min": "60",
										"minMathNotation": "greaterOrEqual"
									}
								},
								"valueFormat": "A"
							}
						},
						"choiceType": "1",
						"isRequired": "true"
					}
				},
				"parentPositionInfo": {
					"code": "61.10.30.190-00000011",
					"version": "1"
				},
				"applicationDateStart": "2018-11-20T00:00:00+03:00"
			}
		}
	}, {
		"id": 112906,
		"code": "61.10.30.190-00000056",
		"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
		"data": {
			"data": {
				"NSI": {
					"classifiers": {
						"classifier": [{
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.10.30.190",
										"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
									}
								}
							}, {
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.20.30.120",
										"name": "Услуги по передаче данных по беспроводным телекоммуникационным сетям"
									}
								}
							}
						]
					}
				},
				"code": "61.10.30.190-00000056",
				"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
				"OKEIs": {
					"OKEI": {
						"code": "876",
						"name": "Условная единица"
					}
				},
				"OKPD2": {
					"code": "61.10.30.190",
					"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
				},
				"actual": "true",
				"version": "1",
				"isTemplate": "false",
				"publishDate": "2018-10-11T17:37:01+04:00",
				"inclusionDate": "2018-10-11T17:37:01+04:00",
				"characteristics": {
					"characteristic": {
						"code": "570b2c9f-0",
						"kind": "1",
						"name": "Пропускная способность",
						"type": "2",
						"actual": "true",
						"values": {
							"value": {
								"OKEI": {
									"code": "2545",
									"name": "Мегабит в секунду"
								},
								"rangeSet": {
									"valueRange": {
										"min": "50",
										"minMathNotation": "greaterOrEqual"
									}
								},
								"valueFormat": "A"
							}
						},
						"choiceType": "1",
						"isRequired": "true"
					}
				},
				"parentPositionInfo": {
					"code": "61.10.30.190-00000011",
					"version": "1"
				},
				"applicationDateStart": "2018-11-20T00:00:00+03:00"
			}
		}
	}, {
		"id": 112905,
		"code": "61.10.30.190-00000057",
		"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
		"data": {
			"data": {
				"NSI": {
					"classifiers": {
						"classifier": [{
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.10.30.190",
										"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
									}
								}
							}, {
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.20.30.120",
										"name": "Услуги по передаче данных по беспроводным телекоммуникационным сетям"
									}
								}
							}
						]
					}
				},
				"code": "61.10.30.190-00000057",
				"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
				"OKEIs": {
					"OKEI": {
						"code": "876",
						"name": "Условная единица"
					}
				},
				"OKPD2": {
					"code": "61.10.30.190",
					"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
				},
				"actual": "true",
				"version": "1",
				"isTemplate": "false",
				"publishDate": "2018-10-11T17:37:02+04:00",
				"inclusionDate": "2018-10-11T17:37:02+04:00",
				"characteristics": {
					"characteristic": {
						"code": "570b2c9f-0",
						"kind": "1",
						"name": "Пропускная способность",
						"type": "2",
						"actual": "true",
						"values": {
							"value": {
								"OKEI": {
									"code": "2545",
									"name": "Мегабит в секунду"
								},
								"rangeSet": {
									"valueRange": {
										"min": "40",
										"minMathNotation": "greaterOrEqual"
									}
								},
								"valueFormat": "A"
							}
						},
						"choiceType": "1",
						"isRequired": "true"
					}
				},
				"parentPositionInfo": {
					"code": "61.10.30.190-00000011",
					"version": "1"
				},
				"applicationDateStart": "2018-11-20T00:00:00+03:00"
			}
		}
	}, {
		"id": 112904,
		"code": "61.10.30.190-00000058",
		"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
		"data": {
			"data": {
				"NSI": {
					"classifiers": {
						"classifier": [{
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.10.30.190",
										"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
									}
								}
							}, {
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.20.30.120",
										"name": "Услуги по передаче данных по беспроводным телекоммуникационным сетям"
									}
								}
							}
						]
					}
				},
				"code": "61.10.30.190-00000058",
				"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
				"OKEIs": {
					"OKEI": {
						"code": "876",
						"name": "Условная единица"
					}
				},
				"OKPD2": {
					"code": "61.10.30.190",
					"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
				},
				"actual": "true",
				"version": "1",
				"isTemplate": "false",
				"publishDate": "2018-10-11T17:37:02+04:00",
				"inclusionDate": "2018-10-11T17:37:02+04:00",
				"characteristics": {
					"characteristic": {
						"code": "570b2c9f-0",
						"kind": "1",
						"name": "Пропускная способность",
						"type": "2",
						"actual": "true",
						"values": {
							"value": {
								"OKEI": {
									"code": "2545",
									"name": "Мегабит в секунду"
								},
								"rangeSet": {
									"valueRange": {
										"min": "30",
										"minMathNotation": "greaterOrEqual"
									}
								},
								"valueFormat": "A"
							}
						},
						"choiceType": "1",
						"isRequired": "true"
					}
				},
				"parentPositionInfo": {
					"code": "61.10.30.190-00000011",
					"version": "1"
				},
				"applicationDateStart": "2018-11-20T00:00:00+03:00"
			}
		}
	}, {
		"id": 112903,
		"code": "61.10.30.190-00000059",
		"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
		"data": {
			"data": {
				"NSI": {
					"classifiers": {
						"classifier": [{
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.10.30.190",
										"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
									}
								}
							}, {
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.20.30.120",
										"name": "Услуги по передаче данных по беспроводным телекоммуникационным сетям"
									}
								}
							}
						]
					}
				},
				"code": "61.10.30.190-00000059",
				"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
				"OKEIs": {
					"OKEI": {
						"code": "876",
						"name": "Условная единица"
					}
				},
				"OKPD2": {
					"code": "61.10.30.190",
					"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
				},
				"actual": "true",
				"version": "1",
				"isTemplate": "false",
				"publishDate": "2018-10-11T17:37:03+04:00",
				"inclusionDate": "2018-10-11T17:37:03+04:00",
				"characteristics": {
					"characteristic": {
						"code": "570b2c9f-0",
						"kind": "1",
						"name": "Пропускная способность",
						"type": "2",
						"actual": "true",
						"values": {
							"value": {
								"OKEI": {
									"code": "2545",
									"name": "Мегабит в секунду"
								},
								"rangeSet": {
									"valueRange": {
										"min": "20",
										"minMathNotation": "greaterOrEqual"
									}
								},
								"valueFormat": "A"
							}
						},
						"choiceType": "1",
						"isRequired": "true"
					}
				},
				"parentPositionInfo": {
					"code": "61.10.30.190-00000011",
					"version": "1"
				},
				"applicationDateStart": "2018-11-20T00:00:00+03:00"
			}
		}
	}, {
		"id": 112902,
		"code": "61.10.30.190-00000060",
		"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
		"data": {
			"data": {
				"NSI": {
					"classifiers": {
						"classifier": [{
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.10.30.190",
										"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
									}
								}
							}, {
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.20.30.120",
										"name": "Услуги по передаче данных по беспроводным телекоммуникационным сетям"
									}
								}
							}
						]
					}
				},
				"code": "61.10.30.190-00000060",
				"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
				"OKEIs": {
					"OKEI": {
						"code": "876",
						"name": "Условная единица"
					}
				},
				"OKPD2": {
					"code": "61.10.30.190",
					"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
				},
				"actual": "true",
				"version": "1",
				"isTemplate": "false",
				"publishDate": "2018-10-11T17:37:04+04:00",
				"inclusionDate": "2018-10-11T17:37:04+04:00",
				"characteristics": {
					"characteristic": {
						"code": "570b2c9f-0",
						"kind": "1",
						"name": "Пропускная способность",
						"type": "2",
						"actual": "true",
						"values": {
							"value": {
								"OKEI": {
									"code": "2545",
									"name": "Мегабит в секунду"
								},
								"rangeSet": {
									"valueRange": {
										"min": "10",
										"minMathNotation": "greaterOrEqual"
									}
								},
								"valueFormat": "A"
							}
						},
						"choiceType": "1",
						"isRequired": "true"
					}
				},
				"parentPositionInfo": {
					"code": "61.10.30.190-00000011",
					"version": "1"
				},
				"applicationDateStart": "2018-11-20T00:00:00+03:00"
			}
		}
	}, {
		"id": 112901,
		"code": "61.10.30.190-00000061",
		"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
		"data": {
			"data": {
				"NSI": {
					"classifiers": {
						"classifier": [{
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.10.30.190",
										"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
									}
								}
							}, {
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.20.30.120",
										"name": "Услуги по передаче данных по беспроводным телекоммуникационным сетям"
									}
								}
							}
						]
					}
				},
				"code": "61.10.30.190-00000061",
				"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
				"OKEIs": {
					"OKEI": {
						"code": "876",
						"name": "Условная единица"
					}
				},
				"OKPD2": {
					"code": "61.10.30.190",
					"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
				},
				"actual": "true",
				"version": "1",
				"isTemplate": "false",
				"publishDate": "2018-10-11T17:37:04+04:00",
				"inclusionDate": "2018-10-11T17:37:04+04:00",
				"characteristics": {
					"characteristic": {
						"code": "570b2c9f-0",
						"kind": "1",
						"name": "Пропускная способность",
						"type": "2",
						"actual": "true",
						"values": {
							"value": {
								"OKEI": {
									"code": "2545",
									"name": "Мегабит в секунду"
								},
								"rangeSet": {
									"valueRange": {
										"min": "9",
										"minMathNotation": "greaterOrEqual"
									}
								},
								"valueFormat": "A"
							}
						},
						"choiceType": "1",
						"isRequired": "true"
					}
				},
				"parentPositionInfo": {
					"code": "61.10.30.190-00000011",
					"version": "1"
				},
				"applicationDateStart": "2018-11-20T00:00:00+03:00"
			}
		}
	}, {
		"id": 112900,
		"code": "61.10.30.190-00000062",
		"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
		"data": {
			"data": {
				"NSI": {
					"classifiers": {
						"classifier": [{
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.10.30.190",
										"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
									}
								}
							}, {
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.20.30.120",
										"name": "Услуги по передаче данных по беспроводным телекоммуникационным сетям"
									}
								}
							}
						]
					}
				},
				"code": "61.10.30.190-00000062",
				"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
				"OKEIs": {
					"OKEI": {
						"code": "876",
						"name": "Условная единица"
					}
				},
				"OKPD2": {
					"code": "61.10.30.190",
					"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
				},
				"actual": "true",
				"version": "1",
				"isTemplate": "false",
				"publishDate": "2018-10-11T17:37:05+04:00",
				"inclusionDate": "2018-10-11T17:37:05+04:00",
				"characteristics": {
					"characteristic": {
						"code": "570b2c9f-0",
						"kind": "1",
						"name": "Пропускная способность",
						"type": "2",
						"actual": "true",
						"values": {
							"value": {
								"OKEI": {
									"code": "2545",
									"name": "Мегабит в секунду"
								},
								"rangeSet": {
									"valueRange": {
										"min": "8",
										"minMathNotation": "greaterOrEqual"
									}
								},
								"valueFormat": "A"
							}
						},
						"choiceType": "1",
						"isRequired": "true"
					}
				},
				"parentPositionInfo": {
					"code": "61.10.30.190-00000011",
					"version": "1"
				},
				"applicationDateStart": "2018-11-20T00:00:00+03:00"
			}
		}
	}, {
		"id": 112899,
		"code": "61.10.30.190-00000063",
		"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
		"data": {
			"data": {
				"NSI": {
					"classifiers": {
						"classifier": [{
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.10.30.190",
										"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
									}
								}
							}, {
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.20.30.120",
										"name": "Услуги по передаче данных по беспроводным телекоммуникационным сетям"
									}
								}
							}
						]
					}
				},
				"code": "61.10.30.190-00000063",
				"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
				"OKEIs": {
					"OKEI": {
						"code": "876",
						"name": "Условная единица"
					}
				},
				"OKPD2": {
					"code": "61.10.30.190",
					"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
				},
				"actual": "true",
				"version": "1",
				"isTemplate": "false",
				"publishDate": "2018-10-11T17:37:05+04:00",
				"inclusionDate": "2018-10-11T17:37:05+04:00",
				"characteristics": {
					"characteristic": {
						"code": "570b2c9f-0",
						"kind": "1",
						"name": "Пропускная способность",
						"type": "2",
						"actual": "true",
						"values": {
							"value": {
								"OKEI": {
									"code": "2545",
									"name": "Мегабит в секунду"
								},
								"rangeSet": {
									"valueRange": {
										"min": "7",
										"minMathNotation": "greaterOrEqual"
									}
								},
								"valueFormat": "A"
							}
						},
						"choiceType": "1",
						"isRequired": "true"
					}
				},
				"parentPositionInfo": {
					"code": "61.10.30.190-00000011",
					"version": "1"
				},
				"applicationDateStart": "2018-11-20T00:00:00+03:00"
			}
		}
	}, {
		"id": 112898,
		"code": "61.10.30.190-00000064",
		"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
		"data": {
			"data": {
				"NSI": {
					"classifiers": {
						"classifier": [{
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.10.30.190",
										"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
									}
								}
							}, {
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.20.30.120",
										"name": "Услуги по передаче данных по беспроводным телекоммуникационным сетям"
									}
								}
							}
						]
					}
				},
				"code": "61.10.30.190-00000064",
				"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
				"OKEIs": {
					"OKEI": {
						"code": "876",
						"name": "Условная единица"
					}
				},
				"OKPD2": {
					"code": "61.10.30.190",
					"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
				},
				"actual": "true",
				"version": "1",
				"isTemplate": "false",
				"publishDate": "2018-10-11T17:37:06+04:00",
				"inclusionDate": "2018-10-11T17:37:06+04:00",
				"characteristics": {
					"characteristic": {
						"code": "570b2c9f-0",
						"kind": "1",
						"name": "Пропускная способность",
						"type": "2",
						"actual": "true",
						"values": {
							"value": {
								"OKEI": {
									"code": "2545",
									"name": "Мегабит в секунду"
								},
								"rangeSet": {
									"valueRange": {
										"min": "6",
										"minMathNotation": "greaterOrEqual"
									}
								},
								"valueFormat": "A"
							}
						},
						"choiceType": "1",
						"isRequired": "true"
					}
				},
				"parentPositionInfo": {
					"code": "61.10.30.190-00000011",
					"version": "1"
				},
				"applicationDateStart": "2018-11-20T00:00:00+03:00"
			}
		}
	}, {
		"id": 112897,
		"code": "61.10.30.190-00000065",
		"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
		"data": {
			"data": {
				"NSI": {
					"classifiers": {
						"classifier": [{
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.10.30.190",
										"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
									}
								}
							}, {
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.20.30.120",
										"name": "Услуги по передаче данных по беспроводным телекоммуникационным сетям"
									}
								}
							}
						]
					}
				},
				"code": "61.10.30.190-00000065",
				"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
				"OKEIs": {
					"OKEI": {
						"code": "876",
						"name": "Условная единица"
					}
				},
				"OKPD2": {
					"code": "61.10.30.190",
					"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
				},
				"actual": "true",
				"version": "1",
				"isTemplate": "false",
				"publishDate": "2018-10-11T17:37:07+04:00",
				"inclusionDate": "2018-10-11T17:37:07+04:00",
				"characteristics": {
					"characteristic": {
						"code": "570b2c9f-0",
						"kind": "1",
						"name": "Пропускная способность",
						"type": "2",
						"actual": "true",
						"values": {
							"value": {
								"OKEI": {
									"code": "2545",
									"name": "Мегабит в секунду"
								},
								"rangeSet": {
									"valueRange": {
										"min": "4",
										"minMathNotation": "greaterOrEqual"
									}
								},
								"valueFormat": "A"
							}
						},
						"choiceType": "1",
						"isRequired": "true"
					}
				},
				"parentPositionInfo": {
					"code": "61.10.30.190-00000011",
					"version": "1"
				},
				"applicationDateStart": "2018-11-20T00:00:00+03:00"
			}
		}
	}, {
		"id": 112896,
		"code": "61.10.30.190-00000066",
		"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
		"data": {
			"data": {
				"NSI": {
					"classifiers": {
						"classifier": [{
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.10.30.190",
										"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
									}
								}
							}, {
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.20.30.120",
										"name": "Услуги по передаче данных по беспроводным телекоммуникационным сетям"
									}
								}
							}
						]
					}
				},
				"code": "61.10.30.190-00000066",
				"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
				"OKEIs": {
					"OKEI": {
						"code": "876",
						"name": "Условная единица"
					}
				},
				"OKPD2": {
					"code": "61.10.30.190",
					"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
				},
				"actual": "true",
				"version": "1",
				"isTemplate": "false",
				"publishDate": "2018-10-11T17:37:07+04:00",
				"inclusionDate": "2018-10-11T17:37:07+04:00",
				"characteristics": {
					"characteristic": {
						"code": "570b2c9f-0",
						"kind": "1",
						"name": "Пропускная способность",
						"type": "2",
						"actual": "true",
						"values": {
							"value": {
								"OKEI": {
									"code": "2545",
									"name": "Мегабит в секунду"
								},
								"rangeSet": {
									"valueRange": {
										"min": "3",
										"minMathNotation": "greaterOrEqual"
									}
								},
								"valueFormat": "A"
							}
						},
						"choiceType": "1",
						"isRequired": "true"
					}
				},
				"parentPositionInfo": {
					"code": "61.10.30.190-00000011",
					"version": "1"
				},
				"applicationDateStart": "2018-11-20T00:00:00+03:00"
			}
		}
	}, {
		"id": 112895,
		"code": "61.10.30.190-00000067",
		"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
		"data": {
			"data": {
				"NSI": {
					"classifiers": {
						"classifier": [{
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.10.30.190",
										"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
									}
								}
							}, {
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.20.30.120",
										"name": "Услуги по передаче данных по беспроводным телекоммуникационным сетям"
									}
								}
							}
						]
					}
				},
				"code": "61.10.30.190-00000067",
				"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
				"OKEIs": {
					"OKEI": {
						"code": "876",
						"name": "Условная единица"
					}
				},
				"OKPD2": {
					"code": "61.10.30.190",
					"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
				},
				"actual": "true",
				"version": "1",
				"isTemplate": "false",
				"publishDate": "2018-10-11T17:37:08+04:00",
				"inclusionDate": "2018-10-11T17:37:08+04:00",
				"characteristics": {
					"characteristic": {
						"code": "570b2c9f-0",
						"kind": "1",
						"name": "Пропускная способность",
						"type": "2",
						"actual": "true",
						"values": {
							"value": {
								"OKEI": {
									"code": "2545",
									"name": "Мегабит в секунду"
								},
								"rangeSet": {
									"valueRange": {
										"min": "2",
										"minMathNotation": "greaterOrEqual"
									}
								},
								"valueFormat": "A"
							}
						},
						"choiceType": "1",
						"isRequired": "true"
					}
				},
				"parentPositionInfo": {
					"code": "61.10.30.190-00000011",
					"version": "1"
				},
				"applicationDateStart": "2018-11-20T00:00:00+03:00"
			}
		}
	}, {
		"id": 112894,
		"code": "61.10.30.190-00000068",
		"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
		"data": {
			"data": {
				"NSI": {
					"classifiers": {
						"classifier": [{
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.10.30.190",
										"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
									}
								}
							}, {
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.20.30.120",
										"name": "Услуги по передаче данных по беспроводным телекоммуникационным сетям"
									}
								}
							}
						]
					}
				},
				"code": "61.10.30.190-00000068",
				"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
				"OKEIs": {
					"OKEI": {
						"code": "876",
						"name": "Условная единица"
					}
				},
				"OKPD2": {
					"code": "61.10.30.190",
					"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
				},
				"actual": "true",
				"version": "1",
				"isTemplate": "false",
				"publishDate": "2018-10-11T17:37:08+04:00",
				"inclusionDate": "2018-10-11T17:37:08+04:00",
				"characteristics": {
					"characteristic": {
						"code": "570b2c9f-0",
						"kind": "1",
						"name": "Пропускная способность",
						"type": "2",
						"actual": "true",
						"values": {
							"value": {
								"OKEI": {
									"code": "2545",
									"name": "Мегабит в секунду"
								},
								"rangeSet": {
									"valueRange": {
										"min": "1",
										"minMathNotation": "greaterOrEqual"
									}
								},
								"valueFormat": "A"
							}
						},
						"choiceType": "1",
						"isRequired": "true"
					}
				},
				"parentPositionInfo": {
					"code": "61.10.30.190-00000011",
					"version": "1"
				},
				"applicationDateStart": "2018-11-20T00:00:00+03:00"
			}
		}
	}, {
		"id": 112893,
		"code": "61.10.30.190-00000069",
		"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
		"data": {
			"data": {
				"NSI": {
					"classifiers": {
						"classifier": [{
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.10.30.190",
										"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
									}
								}
							}, {
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.20.30.120",
										"name": "Услуги по передаче данных по беспроводным телекоммуникационным сетям"
									}
								}
							}
						]
					}
				},
				"code": "61.10.30.190-00000069",
				"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
				"OKEIs": {
					"OKEI": {
						"code": "876",
						"name": "Условная единица"
					}
				},
				"OKPD2": {
					"code": "61.10.30.190",
					"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
				},
				"actual": "true",
				"version": "1",
				"isTemplate": "false",
				"publishDate": "2018-10-11T17:37:09+04:00",
				"inclusionDate": "2018-10-11T17:37:09+04:00",
				"characteristics": {
					"characteristic": {
						"code": "570b2c9f-0",
						"kind": "1",
						"name": "Пропускная способность",
						"type": "2",
						"actual": "true",
						"values": {
							"value": {
								"OKEI": {
									"code": "2545",
									"name": "Мегабит в секунду"
								},
								"rangeSet": {
									"valueRange": {
										"min": ".512",
										"minMathNotation": "greaterOrEqual"
									}
								},
								"valueFormat": "A"
							}
						},
						"choiceType": "1",
						"isRequired": "true"
					}
				},
				"parentPositionInfo": {
					"code": "61.10.30.190-00000011",
					"version": "1"
				},
				"applicationDateStart": "2018-11-20T00:00:00+03:00"
			}
		}
	}, {
		"id": 112892,
		"code": "61.10.30.190-00000070",
		"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
		"data": {
			"data": {
				"NSI": {
					"classifiers": {
						"classifier": [{
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.10.30.190",
										"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
									}
								}
							}, {
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.20.30.120",
										"name": "Услуги по передаче данных по беспроводным телекоммуникационным сетям"
									}
								}
							}
						]
					}
				},
				"code": "61.10.30.190-00000070",
				"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
				"OKEIs": {
					"OKEI": {
						"code": "876",
						"name": "Условная единица"
					}
				},
				"OKPD2": {
					"code": "61.10.30.190",
					"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
				},
				"actual": "true",
				"version": "1",
				"isTemplate": "false",
				"publishDate": "2018-10-11T17:37:09+04:00",
				"inclusionDate": "2018-10-11T17:37:09+04:00",
				"characteristics": {
					"characteristic": {
						"code": "570b2c9f-0",
						"kind": "1",
						"name": "Пропускная способность",
						"type": "2",
						"actual": "true",
						"values": {
							"value": {
								"OKEI": {
									"code": "2545",
									"name": "Мегабит в секунду"
								},
								"rangeSet": {
									"valueRange": {
										"min": ".256",
										"minMathNotation": "greaterOrEqual"
									}
								},
								"valueFormat": "A"
							}
						},
						"choiceType": "1",
						"isRequired": "true"
					}
				},
				"parentPositionInfo": {
					"code": "61.10.30.190-00000011",
					"version": "1"
				},
				"applicationDateStart": "2018-11-20T00:00:00+03:00"
			}
		}
	}, {
		"id": 112891,
		"code": "61.10.30.190-00000071",
		"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
		"data": {
			"data": {
				"NSI": {
					"classifiers": {
						"classifier": [{
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.10.30.190",
										"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
									}
								}
							}, {
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.20.30.120",
										"name": "Услуги по передаче данных по беспроводным телекоммуникационным сетям"
									}
								}
							}
						]
					}
				},
				"code": "61.10.30.190-00000071",
				"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
				"OKEIs": {
					"OKEI": {
						"code": "876",
						"name": "Условная единица"
					}
				},
				"OKPD2": {
					"code": "61.10.30.190",
					"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
				},
				"actual": "true",
				"version": "1",
				"isTemplate": "false",
				"publishDate": "2018-10-11T17:37:10+04:00",
				"inclusionDate": "2018-10-11T17:37:10+04:00",
				"characteristics": {
					"characteristic": {
						"code": "570b2c9f-0",
						"kind": "1",
						"name": "Пропускная способность",
						"type": "2",
						"actual": "true",
						"values": {
							"value": {
								"OKEI": {
									"code": "2545",
									"name": "Мегабит в секунду"
								},
								"rangeSet": {
									"valueRange": {
										"min": ".128",
										"minMathNotation": "greaterOrEqual"
									}
								},
								"valueFormat": "A"
							}
						},
						"choiceType": "1",
						"isRequired": "true"
					}
				},
				"parentPositionInfo": {
					"code": "61.10.30.190-00000011",
					"version": "1"
				},
				"applicationDateStart": "2018-11-20T00:00:00+03:00"
			}
		}
	}, {
		"id": 112890,
		"code": "61.10.30.190-00000072",
		"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
		"data": {
			"data": {
				"NSI": {
					"classifiers": {
						"classifier": [{
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.10.30.190",
										"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
									}
								}
							}, {
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.20.30.120",
										"name": "Услуги по передаче данных по беспроводным телекоммуникационным сетям"
									}
								}
							}
						]
					}
				},
				"code": "61.10.30.190-00000072",
				"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
				"OKEIs": {
					"OKEI": {
						"code": "876",
						"name": "Условная единица"
					}
				},
				"OKPD2": {
					"code": "61.10.30.190",
					"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
				},
				"actual": "true",
				"version": "1",
				"isTemplate": "false",
				"publishDate": "2018-10-11T17:37:10+04:00",
				"inclusionDate": "2018-10-11T17:37:10+04:00",
				"characteristics": {
					"characteristic": {
						"code": "570b2c9f-0",
						"kind": "1",
						"name": "Пропускная способность",
						"type": "2",
						"actual": "true",
						"values": {
							"value": {
								"OKEI": {
									"code": "2545",
									"name": "Мегабит в секунду"
								},
								"rangeSet": {
									"valueRange": {
										"min": ".064",
										"minMathNotation": "greaterOrEqual"
									}
								},
								"valueFormat": "A"
							}
						},
						"choiceType": "1",
						"isRequired": "true"
					}
				},
				"parentPositionInfo": {
					"code": "61.10.30.190-00000011",
					"version": "1"
				},
				"applicationDateStart": "2018-11-20T00:00:00+03:00"
			}
		}
	}, {
		"id": 112889,
		"code": "61.10.30.190-00000073",
		"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
		"data": {
			"data": {
				"NSI": {
					"classifiers": {
						"classifier": [{
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.10.30.190",
										"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
									}
								}
							}, {
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.20.30.120",
										"name": "Услуги по передаче данных по беспроводным телекоммуникационным сетям"
									}
								}
							}
						]
					}
				},
				"code": "61.10.30.190-00000073",
				"name": "Услуги по предоставлению канала доступа к виртуальным частным cетям (VPN)",
				"OKEIs": {
					"OKEI": {
						"code": "876",
						"name": "Условная единица"
					}
				},
				"OKPD2": {
					"code": "61.10.30.190",
					"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
				},
				"actual": "true",
				"version": "1",
				"isTemplate": "false",
				"publishDate": "2018-10-11T17:37:11+04:00",
				"inclusionDate": "2018-10-11T17:37:11+04:00",
				"characteristics": {
					"characteristic": {
						"code": "570b2c9f-0",
						"kind": "1",
						"name": "Пропускная способность",
						"type": "2",
						"actual": "true",
						"values": {
							"value": {
								"OKEI": {
									"code": "2545",
									"name": "Мегабит в секунду"
								},
								"rangeSet": {
									"valueRange": {
										"min": ".016",
										"minMathNotation": "greaterOrEqual"
									}
								},
								"valueFormat": "A"
							}
						},
						"choiceType": "1",
						"isRequired": "true"
					}
				},
				"parentPositionInfo": {
					"code": "61.10.30.190-00000011",
					"version": "1"
				},
				"applicationDateStart": "2018-11-20T00:00:00+03:00"
			}
		}
	}, {
		"id": 112962,
		"code": "61.10.30.190-00000074",
		"name": "Услуги по предоставлению виртуального выделенного канала Ethernet",
		"data": {
			"data": {
				"NSI": {
					"classifiers": {
						"classifier": [{
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.10.30.190",
										"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
									}
								}
							}, {
								"name": "Общероссийский классификатор продукции по видам экономической деятельности (ОКПД2)",
								"values": {
									"value": {
										"code": "61.20.30.120",
										"name": "Услуги по передаче данных по беспроводным телекоммуникационным сетям"
									}
								}
							}
						]
					}
				},
				"code": "61.10.30.190-00000074",
				"name": "Услуги по предоставлению виртуального выделенного канала Ethernet",
				"OKEIs": {
					"OKEI": {
						"code": "876",
						"name": "Условная единица"
					}
				},
				"OKPD2": {
					"code": "61.10.30.190",
					"name": "Услуги по передаче данных по проводным телекоммуникационным сетям прочие"
				},
				"actual": "true",
				"version": "1",
				"isTemplate": "true",
				"publishDate": "2018-10-11T17:55:07+04:00",
				"inclusionDate": "2018-10-11T17:55:07+04:00",
				"characteristics": {
					"characteristic": {
						"code": "4d177152-b",
						"kind": "1",
						"name": "Пропускная способность",
						"type": "2",
						"actual": "true",
						"values": {
							"value": [{
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "100000",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "40000",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "20000",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "10000",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "9000",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "8000",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "7000",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "6000",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "5000",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "4000",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "3000",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "2000",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "1000",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "950",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "900",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "850",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "800",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "750",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "700",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "650",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "600",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "550",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "500",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "450",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "400",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "350",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "300",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "250",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "200",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "190",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "180",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "170",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "160",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "150",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "140",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "130",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "120",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "110",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "100",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "90",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "80",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "70",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "60",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "50",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "40",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "30",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "20",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "10",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "9",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "8",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "7",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "6",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "5",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "4",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "3",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "2",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": "1",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": ".512",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": ".256",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": ".128",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": ".064",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}, {
									"OKEI": {
										"code": "2545",
										"name": "Мегабит в секунду"
									},
									"rangeSet": {
										"valueRange": {
											"min": ".016",
											"minMathNotation": "greaterOrEqual"
										}
									},
									"valueFormat": "A"
								}
							]
						},
						"choiceType": "1",
						"isRequired": "true"
					}
				},
				"applicationDateStart": "2018-11-20T00:00:00+03:00"
			}
		}
	}
]